let express = require('express')
let mongodb = require('mongodb')
let client = mongodb.MongoClient
const ObjectId = mongodb.ObjectId

let update = express.Router().put('/:id',(req,res)=>{
    let data = req.body
    let objectId = ObjectId(req.params.id)
    client.connect("mongodb://localhost:27017/talentsprint",(err,db)=>{
        if(err){
            throw err
        }else{
            db.collection('userDetails').updateOne({_id:objectId},{$set:data},(err,result)=>{
                if(err){
                    throw err
                }else{
                    res.send(result)
                }
            })
        }
    })
})

module.exports = update