// Inside App.js
import './App.css';
import Navbar from './components/Navbar';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Main from './components/Main';
import JavaScript from './components/JavaScript'; 
import Login from './components/Login';
import Register from './components/Register';
import { useState, useEffect  } from 'react';
import Home from './components/Home';
import NavbarForAdmin from './components/NavbarForAdmin';
import Admin from './components/Admin';
import ReactLearning from './components/React';
import Java from './components/Java';
import UpdateDetails from './components/UpdateDetails';

function App() {
  const [isAuthenticated,setIsAuthenticated] = useState(false)
  const [isAdmin,setIsAdmin] = useState(false)
  const [profileupdate,setProfileUpdate] = useState([{}])

  useEffect(() => {
    localStorage.setItem('isAuthenticated', isAuthenticated);
    localStorage.setItem('isAdmin', isAdmin);
    localStorage.setItem('profileupdate', JSON.stringify(profileupdate));
  }, [isAuthenticated, isAdmin, profileupdate]);


  function profileUpdate(data){
      setProfileUpdate(data)
  }
  return (
    <div className="App">
      <BrowserRouter>
         {isAuthenticated && <Navbar />}
         { isAdmin && <NavbarForAdmin/> }
        <Routes>
          <Route path="/*" element={<Main setIsAuthenticated={setIsAuthenticated} setIsAdmin={setIsAdmin}/>} />
          <Route path="/nav/*" element={<Navbar />} />
          <Route path="/adminnav/*" element={<NavbarForAdmin/>}/>
          <Route path='/login' element={<Login setIsAuthenticated={setIsAuthenticated} setIsAdmin={setIsAdmin} profileUpdate={profileUpdate}/>}/>
         <Route path='/register' element={<Register />}/>
         <Route path='/admin' element={<Admin setIsAdmin={setIsAdmin} />}/> 
         <Route path='/userupdate' element={<UpdateDetails profileupdate={profileupdate} setIsAuthenticated={setIsAuthenticated}/>}/>
         {/* <Route path="/JavaScript/*" element={<JavaScript />} />
          <Route path="/java" element={<Java />} />
          <Route path="/nav" element={<Navbar />} />
          <Route path="/sql" element={<SQL />} />
          <Route path="/react/*" element={<ReactLearning />} />
            <Route path='/home' element={<Home/>}/> */}
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
