import React from 'react'

function JSArrayMethods() {
    return (
        <div id='#JSArrays'>
            <h2>JavaScript Array Methods</h2>
            <p>JavaScrip Array methods are used to <strong> add, remove, iterate, or manipulate data </strong>in the array as per our requirements.</p>
            <h6>Following are some Array Methods :</h6>
            <ul>
                <li><strong>Array.length Method</strong></li>
                <li><strong>Array.toString() Method</strong></li>
                <li><strong>Array.push() Method</strong></li>
                <li><strong>Array.pop() Method</strong></li>
                <li><strong>Array.shift() Method</strong></li>
                <li><strong>Array.unshift() Method</strong></li>
                <li><strong>Array.splice()  Method</strong></li>
                <li><strong>Array.slice() Method</strong></li>
                <li><strong>Array.join() Method</strong></li>
                <li><strong>Array.concat() Method</strong></li>
            </ul>
            <h5>1. JavaScript Array.length</h5>
            <p>The length property returns the length (size) of an array:</p>
            <h6>Example</h6>
            <pre>
                <code>
                    const fruits = ["Banana", "Orange", "Apple", "Mango"];
                    <br />
                    let size = fruits.length;
                    <br />
                    console.log(size);
                </code>
            </pre>
            <h6>Output</h6>
            <pre>4</pre>
            <h5>2. JavaScript Array.toString()</h5>
            <p>The JavaScript method toString() converts an <strong>array to a string</strong>.</p>
            <h6>Example</h6>
            <pre>
                <code>
                    const fruits = ["Banana", "Orange", "Apple", "Mango"]; <br />
                    consile.log(fruits.toString());
                </code>
            </pre>
            <h6>Output</h6>
            <pre>Banana,Orange,Apple,Mango</pre> <br />
            <h5>3. Javascript Array.push() Method</h5>
            <p>The push() method adds a <strong>new element</strong> to an array <strong>at the end</strong></p>
            <h6>Example</h6>
            <pre>
                <code>
                    let numbers = [10, 20, 30, 40, 50];
                    <br />
                    console.log(numbers);
                    <br />
                    numbers.push(60);
                    <br />
                    console.log(numbers);
                </code>
            </pre>
            <h6>Output</h6>
            <pre>[10, 20, 30, 40, 50] <br />
                [10, 20, 30, 40, 50, 60]
            </pre><br />
            <h5>4. JavaScript Array.pop() Method</h5>
            <p>The pop() method <strong>removes the last element</strong> from an array.</p>
            <h6>Example</h6>
            <pre>
                <code>
                    let number_arr = [20, 30, 40, 50];
                    <br />
                    console.log(number_arr);
                    <br />
                    number_arr.pop();
                    <br />
                    console.log(number_arr);
                </code>
            </pre>
            <h6>Output</h6>
            <pre>
                [20, 30, 40, 50] <br />
                [20, 30, 40]
            </pre><br />
            <h5>5. JavaScript Array.shift() Method</h5>
            <p>The shift() method <strong> removes the first array element</strong> and "shifts" all other elements to a lower index.</p>
            <h6>Example</h6>
            <pre>
                <code>
                    let arr = [20, 30, 40, 50, 60];
                    <br />
                    console.log(arr);
                    <br />
                    arr.shift();
                    <br />
                    console.log(arr);
                </code>
            </pre>

            <h6>Output</h6>
            <pre>
                [20, 30, 40, 50, 60] <br />
                [30, 40, 50, 60]
            </pre><br />
            <h5>6. JavaScript Array unshift()</h5>
            <p>The unshift() method adds a <strong> new element</strong> to an array <strong>at the beginning</strong>.</p>
            <h6>Example</h6>
            <pre>
                <code>
                    let arr = [20, 30, 40, 50, 60];
                    <br />
                    console.log(arr);
                    <br />
                    arr.unshift(10);
                    <br />
                    console.log(arr);
                </code>
            </pre>
            <h6>Output</h6>
            <pre>
                [20, 30, 40, 50, 60] <br />
                [10, 20, 30, 40, 50, 60]
            </pre><br />
            <h5>7. JavaScript Array.splice() Method</h5>
            <p>This method is used for the <strong> Insertion and Removal of elements in between an Array. </strong></p>
            <h6>Syntax</h6>
            <pre>
                Array.splice (start, deleteCount, item 1, item 2….)
            </pre>
            <h6>Parameters</h6>
            <ul>
                <li><strong>Start:</strong> Location at which to perform the operation.</li>
                <li><strong>deleteCount:</strong> Number of elements to be deleted, if no element is to be deleted pass 0.</li>
                <li><strong>Item1, item2 …..:</strong> this is an optional parameter. </li>
            </ul>
            <h6>Example</h6>
            <pre>
                <code>
                    let number_arr = [20, 30, 40, 50, 60];
                    <br />
                    number_arr.splice(1, 3);
                    <br />
                    console.log(number_arr);
                    <br />
                    number_arr.splice(1, 0, 3, 4, 5);
                    <br />
                    console.log(number_arr);
                </code>
            </pre>

            <h6>Output</h6>
            <pre>
                [20, 60] <br />
                [ 20, 3, 4, 5, 60 ]
            </pre><br />

            <h5>8. JavaScript Array.slice() Method</h5>
            <p>The slice() method slices out a piece of an array into a new array.</p>
            <h6>Syntax</h6>
            <pre>Array.slice (startIndex , endIndex);</pre>
            <h6>Example</h6>
            <pre>
                <code>
                    const originalArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
                    <br />
                    const case1 = originalArr.slice(0, 3);
                    <br />
                    console.log(case1);
                    <br />
                    const case2 = originalArr.slice(-3);
                    <br />
                    console.log( case2 );
                    <br />
                    const case3 = originalArr.slice(3, 7);
                    <br />
                    console.log( case3 );
                    <br />
                    const case4 = originalArr.slice(5, 2);
                    <br />
                    console.log( case4 );
                    <br />
                    const case5 = originalArr.slice(-4, 9);
                    <br />
                    console.log(case5 );
                    <br />
                    const case6 = originalArr.slice(3, -2);
                    <br />
                    console.log( case6 );
                    <br />
                    const case7 = originalArr.slice(5);
                    <br />
                    console.log( case7 );
                </code>
            </pre>

            <h6>Output</h6>
            <pre>
                [ 1,2,3] <br />
                [8,9,10] <br />
                [4,5,6,7] <br />
                [] <br />
                [7,8,9] <br />
                [4,5,6,7,8] <br />
                [6,7,8,9,10]
            </pre> <br />
            <h5>9. JavaScript Array.join() Method</h5>
            <p>The join() method also joins all array elements into a string.</p>
            <pre>
                <code>
                    const fruits = ["Banana", "Orange", "Apple", "Mango"]; <br />
                    fruits.join(" * "); <br />
                    console.log(fruits);
                </code>
            </pre>
            <h6>Output</h6>
            <pre>
                Banana * Orange * Apple * Mango
            </pre><br />
            <h5>10. JavaScript Array.concat()</h5>
            <p>The concat() method creates a new array by merging (concatenating) existing arrays</p>
            <h6>Example</h6>
            <pre>
                <code>
                    const myGirls = ["Cecilie", "Lone"];
                    <br />
                    const myBoys = ["Emil", "Tobias", "Linus"];
                    <br />
                    const myChildren = myGirls.concat(myBoys);
                </code>
            </pre>
            <h6>Output</h6>
            <pre>
                [Cecilie,Lone,Emil,Tobias,Linus]
            </pre> <br />


        </div>
    )
}

export default JSArrayMethods