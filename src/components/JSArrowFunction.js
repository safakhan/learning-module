import React from 'react'

function JSArrowFunction() {
    return (
        <div>
            <h2>Javascript Arrow Function</h2>
            <p>Arrow function <strong> allow us to write Javascript functions in shorter way.</strong> Arrow functions were <strong> introduced in the ES6 version.</strong></p>
            <p>Arrow functions are <strong> anonymous functions </strong> i.e. functions <strong> without a name</strong> and are not bound by an identifier. Arrow functions <strong> do not return any value</strong> and can be <strong>declared without the function keyword.</strong></p>
            <h6>Syntax :</h6>
            <pre>
                <code>
                    const myfunction = () =&gt; &#123;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;console.log( "Hi Guys!" );<br />
                    &#125;
                </code>
            </pre> <br />
            <h5>Arrow Function without Parameters  </h5>
            <h6>Example :</h6>
            <pre>
                <code>
                    const myfunction = () =&gt; &#123;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;console.log( "Hello World!" );<br />
                    &#125;<br /><br />
                    myfunction();
                </code>
            </pre>
            <h6>Output :</h6>
            <pre>Hello World!</pre><br />
            <h5>Arrow Function with Parameters</h5>
            <h6>Example :</h6>
            <pre>
                <code>
                    const result = ( x, y, z ) =&gt; &#123;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;console.log( x + y + z )<br />
                    &#125;<br /><br />

                    result( 10, 20, 30 );
                </code>
            </pre>
            <h6>Output :</h6>
            <pre>60</pre> <br />
            <h5>Arrow Function with Default Parameters</h5>
            <h6>Example :</h6>
            <pre>
                <code>
                    const myfunc = ( x, y, z = 30 ) =&gt; &#123;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;console.log( x + " " + y + " " + z);<br />
                    &#125;<br /><br />

                    myfunc( 10, 20 );
                </code>
            </pre>

            <h6>Output :</h6>
            <pre>10 20 30</pre>

        </div>
    )
}

export default JSArrowFunction