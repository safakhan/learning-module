import React from 'react'
import './home.css'
import {Link} from 'react-router-dom'
import javascript from '../javascript.avif'
import java from '../java.png'
import react from '../react.png'
import sql from '../SQL.png'

function Home(props) {
       
  return (
    <div> 
        <main className='home'>
          {/* <div className='container'>
            <div className="javascript">
                <Link to='/JavaScript'><img src={javascript} alt="javascript" /></Link>
            </div>
            <div className="react">
            <Link to='/JavaScript'><img src={javascript} alt="javascript" /></Link>
            </div>
            <div className="java">
            <Link to='/JavaScript'><img src={javascript} alt="javascript" /></Link>
            </div>
            <div className="sql">
            <Link to='/JavaScript'><img src={javascript} alt="javascript" /></Link>
            </div>
          </div> */}
          <div className='contentStyle'>
          <h1 style={{fontFamily: " 'Crimson', serif, sans-serif",fontStyle: 'italic',paddingLeft:'60px'}}>Missed out on something?</h1>
        <h1 style={{fontFamily: " 'Crimson', serif, sans-serif",fontStyle: 'italic',paddingLeft:'30px'}}>  It's never too late to catch up!</h1>
        </div>
        </main>
    </div>
  )
}

export default Home