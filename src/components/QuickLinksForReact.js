import React, { useRef } from 'react';
import { Link } from 'react-router-dom';


function QuickLinksForReact() {
    const linkStyle = {
        color: "black",
      };
  return (
    <div className='quicklinks'> 
         <Link style={linkStyle} to="/nav/react/" >Introduction to React</Link><br/><br/>
       <Link style={linkStyle} to="/nav/react/setup" >ReactJS Environment Setup</Link><br/><br/>
      <Link style={linkStyle} to="/nav/react/components" >React Components</Link><br/><br/>
     <Link style={linkStyle} to="/nav/react/classcomponents" >React Class Components</Link><br/><br/>
      {/* <Link style={linkStyle} to="/JavaScript/JSFunctions" >JavaScript Functions</Link><br/><br/>
      <Link style={linkStyle} to="/JavaScript/ConditionalStatements" >Conditional Statements</Link><br/><br/>
      <Link style={linkStyle} to="/JavaScript/JSLoops" >JavaScript Loops</Link><br/><br/>
      <Link style={linkStyle} to="/JavaScript/JSTypeof" >JavaScript typeof</Link><br/><br/>
      <Link style={linkStyle} to="/JavaScript/JSArrowFunction" >JavaScript Arrow Function</Link><br/><br/>
      <Link style={linkStyle} to="/JavaScript/JSArrays" >JavaScript Arrays</Link><br/><br/>
      <Link style={linkStyle} to="/JavaScript/JSArrayMethods" >JavaScript Array Methods</Link><br/><br/>
      <Link style={linkStyle} to="/JavaScript/JSStrings" >JavaScript  Strings</Link><br/><br/>
      <Link style={linkStyle} to="/JavaScript/JSStringMethods" >JavaScript String Methods</Link><br/><br/>
      <Link style={linkStyle} to="/JavaScript/JSFOP" >JavaScript FOP</Link><br/><br/>
      <Link style={linkStyle} to="/JavaScript/ToDoList" >To Do List</Link><br/><br/>    */}
    </div>
  )
}

export default QuickLinksForReact