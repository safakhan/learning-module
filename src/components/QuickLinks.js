// Inside QuickLinks.js
import React, { useRef } from 'react';
import { Link } from 'react-router-dom';

// function scrollToRefs(ref){
//   if(window.innerWidth<=768){
//     if(ref.current){
//       console.log("Ref:", ref); // Log the reference
//       console.log("Ref.current:", ref.current); 
//       ref.current.scrollIntoView({behavior:'smooth'})
//     }
//   }
// }
function scrollToRefs(ref) {
  if (ref.current) {
    ref.current.scrollIntoView({ behavior: 'smooth', block: 'start' });
  }
}

 
function QuickLinks() {
const introRef = useRef(null);
const addJSRef = useRef(null);
const variablesRef = useRef(null);
const dataTypesRef = useRef(null);
const functionsRef = useRef(null);
const conditionalStatementsRef = useRef(null);
const loopsRef = useRef(null);
const typeofRef = useRef(null);
const arraysRef = useRef(null);
const arrowFunctionRef = useRef(null);
const arrayMethodsRef = useRef(null);
const stringsRef = useRef(null);
const stringMethodsRef = useRef(null);
const FOPRef = useRef(null);
const todoRef = useRef(null);

  const linkStyle = {
    color: "black",
  };
  return (
    <div className="quicklinks">
      <Link style={linkStyle} to="/nav/JavaScript/" ref={introRef} onClick={()=>scrollToRefs(introRef)} >Introduction to JavaScript</Link><br/><br/>
      <Link style={linkStyle} to="/nav/JavaScript/AddingJS" ref={addJSRef} onClick={()=>scrollToRefs(addJSRef)}>Adding JavaScript to HTML Document</Link><br/><br/>
      <Link style={linkStyle} to="/nav/JavaScript/JSVariable" ref={variablesRef} onClick={()=>scrollToRefs(variablesRef)}>JavaScript Variables</Link><br/><br/>
      <Link style={linkStyle} to="/nav/JavaScript/JSDataTypes" ref={dataTypesRef} onClick={()=>scrollToRefs(dataTypesRef)}>JavaScript Data Types</Link><br/><br/>
      <Link style={linkStyle} to="/nav/JavaScript/JSFunctions" ref={functionsRef} onClick={()=>scrollToRefs(functionsRef)}>JavaScript Functions</Link><br/><br/>
      <Link style={linkStyle} to="/nav/JavaScript/ConditionalStatements" ref={conditionalStatementsRef} onClick={()=>scrollToRefs(conditionalStatementsRef)}>Conditional Statements</Link><br/><br/>
      <Link style={linkStyle} to="/nav/JavaScript/JSLoops" ref={loopsRef} onClick={()=>scrollToRefs(loopsRef)}>JavaScript Loops</Link><br/><br/>
      <Link style={linkStyle} to="/nav/JavaScript/JSTypeof" ref={typeofRef} onClick={()=>scrollToRefs(typeofRef)}>JavaScript typeof</Link><br/><br/>
      <Link style={linkStyle} to="/nav/JavaScript/JSArrowFunction" ref={arrowFunctionRef} onClick={()=>scrollToRefs(arrowFunctionRef)}>JavaScript Arrow Function</Link><br/><br/>
      <Link style={linkStyle} to="/nav/JavaScript/JSArrays" ref={arraysRef} onClick={()=>scrollToRefs(arraysRef)}>JavaScript Arrays</Link><br/><br/>
      <Link style={linkStyle} to="/nav/JavaScript/JSArrayMethods" ref={arrayMethodsRef} onClick={()=>scrollToRefs(arrayMethodsRef)}>JavaScript Array Methods</Link><br/><br/>
      <Link style={linkStyle} to="/nav/JavaScript/JSStrings" ref={stringsRef} onClick={()=>scrollToRefs(stringsRef)}>JavaScript  Strings</Link><br/><br/>
      <Link style={linkStyle} to="/nav/JavaScript/JSStringMethods" ref={stringMethodsRef} onClick={()=>scrollToRefs(stringMethodsRef)}>JavaScript String Methods</Link><br/><br/>
      <Link style={linkStyle} to="/nav/JavaScript/JSFOP" ref={FOPRef} onClick={()=>scrollToRefs(FOPRef)}>JavaScript FOP</Link><br/><br/>
      <Link style={linkStyle} to="/nav/JavaScript/ToDoList" ref={todoRef} onClick={()=>scrollToRefs(todoRef)}>To Do List</Link><br/><br/>   
    </div>
  );
}

export default QuickLinks;
