// Inside JavaScript.js
import { Route, Routes } from 'react-router-dom';
import QuickLinks from './QuickLinks';
import ConditionalStatements from './ConditionalStatements';
import JSIntroduction from './JSIntroduction';
import AddingJS from './AddingJS';
import JSVariable from './JSVariable';
import JSDataTypes from './JSDataTypes';
import JSFunctions from './JSFunctions';
import JSLoops from './JSLoops'
import JSTypeof from './JSTypeof';
import JSArrowFunction from './JSArrowFunction';
import JSArrays from './JSArrays';
import JSArrayMethods from './JSArrayMethods';
import JSStrings from './JSStrings';
import JSStringMethods from './JSStringMethods';
import FOP from './FOP';
import ToDoList from './ToDoList';
function JavaScript() {
  return (
    <div className="contentContainer">
      <QuickLinks />
      <div className="content">
        <Routes>
          {/* Parent route */}
        <Route path="/" element={<JSIntroduction />} />
        <Route path="/AddingJs" element={<AddingJS />} />
        <Route path="/JSVariable" element={<JSVariable />} />
        <Route path="/JSDataTypes" element={<JSDataTypes/>} />
        <Route path="/JSFunctions" element={<JSFunctions/>} />
        <Route path="/ConditionalStatements" element={<ConditionalStatements />} />
        <Route path="/JSLoops" element={<JSLoops/>} />
        <Route path="/JSTypeof" element={<JSTypeof/>} />
        <Route path="/JSArrowFunction" element={<JSArrowFunction/>} />
        <Route path="/JSArrays" element={<JSArrays/>} />
        <Route path="/JSArrayMethods" element={<JSArrayMethods/>} />
        <Route path="/JSStrings" element={<JSStrings/>} />
        <Route path="/JSStringMethods" element={<JSStringMethods/>} />
        <Route path="/JSFOP" element={<FOP/>} />
        <Route path="/ToDoList" element={<ToDoList/>} /> 
        </Routes>
      </div>
    </div>
  );
}

export default JavaScript;
