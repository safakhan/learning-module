import React from 'react'
import { Route, Routes } from 'react-router-dom';
import HTMLSession1 from './HTMLSession1';
import HTMLSession2 from './HTMLSession2';
import HTMLSession3 from './HTMLSession3';
import HTMLSession4 from './HTMLSession4';
import HTMLSession5 from './HTMLSession5';
import HTMLSession6 from './HTMLSession6';
import HTMLSession7 from './HTMLSession7';
import LinksForHTML from './LinksForHTML'

function HTML() {
  return (
    <div className="contentContainer">
      <LinksForHTML/>
      <div className="content">
        <Routes>
              <Route path='/' element = { <HTMLSession1 /> } />
              <Route path='/session2' element = { <HTMLSession2 /> } />
              <Route path='/session3' element = { <HTMLSession3 /> } />
              <Route path='/session4' element = { <HTMLSession4 /> } />
              <Route path='/session5' element = { <HTMLSession5 /> } />
              <Route path='/session6' element = { <HTMLSession6 /> } />
              <Route path='/session7' element = { <HTMLSession7 /> } />
        </Routes>
         
      </div>
    </div>
  )
}

export default HTML