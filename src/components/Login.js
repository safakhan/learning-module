import axios from 'axios'
import React, { useState } from 'react'
import { Link, Navigate, useNavigate } from 'react-router-dom'
import logo2 from '../logo2.png'
import './login.css'
import logo5 from '../logo5.png'

function Login(props) {
 
    const { setIsAuthenticated, setIsAdmin, profileUpdate } = props
    setIsAuthenticated(false)
    setIsAdmin(false)
    const main = {
        fontFamily: 'Arial, sans-serif',
        backgroundColor: '#f3f3f3'
    }
    const header = {
        backgroundColor: 'white',
        color: '#fff',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: '10px 20px'
    }
    const linkStyle = {
        color: '#5e4e9b',
        textDecoration: 'none',
        marginLeft: '20px',
        fontSize: '20px'
    }
    const container = {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: 'calc(100vh - 80px)',
    }
    const loginContainer = {
        backgroundColor: '#fff',
        borderRadius: '5px',
        padding: '20px',
        boxShadow: '0 0 10px rgba(0, 0, 0, 0.1)',
        width: '400px',
        textAlign: 'center',
    }
    const input = {
        width: '100%',
        padding: '10px',
        margin: '10px 0',
        border: '1px solid #ccc',
        borderRadius: '3px',
        fontSize: '16px',
    }
    const button = {
        backgroundColor: '#5e4e9b',
        color: '#fff',
        padding: '10px 20px',
        border: 'none',
        borderRadius: '3px',
        cursor: 'pointer',
        fontSize: '16px',
        width: '300px',
        marginBottom: '15px'
    }

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const navigate = useNavigate()

    const [showPassword, setShowPassword] = useState(false);

    const togglePasswordVisibility = () => {
        setShowPassword(!showPassword);
      };
      

     const handleLogin = async (event) => {
        event.preventDefault()
        let errormsg = document.getElementById('error')
        try {
            const response = await axios.post('http://localhost:8000/login', { email, password })
            const data = response.data
            if (response.data.message == "Admin logged in") {
                setIsAdmin(true)
                navigate('/adminnav/home')
            } else if (response.data.message == "User not found") {
                errormsg.innerHTML = 'Invalid Credentials'
                      
              }else {
                setIsAuthenticated(true)
                // localStorage.setItem('username', email);
                profileUpdate(data)
                navigate('/nav/home')
                 }
        }catch(error){
            console.log(error)
        }
    }
    return (
        <div style={main}>
            <header style={header}>
                <div className="logo img-fluid">
                <Link to='/' style={linkStyle}><img src={logo2} alt="Logo" /></Link>   
                </div>
                <nav>
                    <Link to='/login' style={linkStyle}>Sign In</Link>
                    <Link to='/register' style={linkStyle}>Register</Link>
                </nav>
            </header>
            <main style={container} className='container'>
                <div className="login-container" style={loginContainer}>
                    <h1 style={{ fontSize: '24px', marginBottom: '20px', color: '#5e4e9b' }}>Welcome to TalentSprint</h1>
                    <form onSubmit={handleLogin}>
                        <p id='error' style={{ color: 'red' }}></p>
                        <input style={input} type="text" placeholder="Email" name='email' value={email} onChange={(e) => { setEmail(e.target.value) }} required />
                        <input style={input}  type={showPassword ? 'text' : 'password'} placeholder="Password" name='password' value={password} onChange={(e) => { setPassword(e.target.value) }} required />
                        <div className="remember-me" style={{ display: 'flex', justifyContent: 'flex-start', marginBottom: '10px' }}>
                            <input style={{ marginRight: '5px' }} type="checkbox" onChange={togglePasswordVisibility} id="remember" />
                            <label for="remember" style={{ marginTop: '6px' }}>Show Password</label>
                            <a href="#" className="forgot-password" style={{ marginTop: '6px', marginLeft: '80px', color: '#5e4e9b' }}>Forgot password?</a>
                        </div>
                        <button style={button} type="submit">Sign In</button>
                    </form>
                    <p>Not a member?<Link to='/register'>Register</Link> </p>
                </div>
            </main>
        </div>
    )
}

export default Login