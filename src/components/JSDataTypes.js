import React from 'react'

function JSDataTypes() {
     
  return (
    <div> 
        <div>
                    <h2>JavaScript Data Types</h2>
                    <p>JavaScript provides different datatypes to hold different values on variables.There are two types of data types in JavaScript. </p>
                    <ul>
                        <li>Primitive data type</li>
                        <li>Non-primitive data type</li>
                    </ul>
                    <h5>Primitive Data Types</h5>
                    <ul>
                        <li>String</li>
                        <li>Number</li>
                        <li>Boolean</li>
                        <li>Undefined</li>
                        <li>Null</li>
                    </ul>
                    <h5>Non-Primitive Data Types</h5>
                    <ul>
                        <li>Object</li>
                        <li>Array</li>
                        <li>Date</li>
                    </ul>
                    <h4>Examples</h4>
                    <pre>
                        // Numbers: <br/>
                        let length = 16;<br />
                        let weight = 7.5;<br /><br />

                        // Strings: <br/>
                        let color = "Yellow";<br />
                        let lastName = "Johnson";<br /><br />

                        // Booleans <br/>
                        let x = true;<br />
                        let y = false;<br /><br />

                        // Object: <br/>
                        const person = &#123;firstName : "John", lastName:"Doe"&#125;;<br /><br />

                        // Array object: <br/>
                        const cars = ["Saab", "Volvo", "BMW"];<br /><br />

                        // Date object: <br/>
                        const date = new Date("2022-03-25");<br />
                    </pre>
                </div>
    </div>
  )
}

export default JSDataTypes