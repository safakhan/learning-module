import React from 'react'
import app from '../app.png'
function SetUp() {
  return (
    <div>
      <h2>ReactJS Environment Setup</h2>
      <p>To run any React application, we need to <strong> first setup a ReactJS Development Environment</strong>. </p>
      <p><strong>Step 1: </strong>Navigate to the folder where you want to create the project and open it in terminal</p>
      <p><strong>Step 2: </strong>In the terminal of the application directory type the following command</p>
      <pre>npx create-react-app my-app</pre>
      <p><strong>Step 3: </strong>Navigate to the newly created folder using the command</p>
      <pre>cd my-app</pre>
      <p><strong>Step 4: </strong>A default application will be created with the following project structure and dependencies</p>
      <div>
      <img src={app} alt="" />
      </div><br />
      <p><strong>Step 5: </strong></p>
      <pre>npm start</pre>
    </div>
  )
}

export default SetUp