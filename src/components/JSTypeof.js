import React from 'react'

function JSTypeof() {
    return (
        <div>
            <h2>JavaScript typeof Operator</h2>
            <p>In JavaScript, the typeof operator <strong> returns the data type of its operand</strong> in the form of a string. The operand can be any object, function, or variable. </p> <br />
            <p>In JavaScript typeof returns the following :</p>
            <ul>
                <li>string</li>
                <li>number</li>
                <li>boolean</li>
                <li>object</li>
                <li>function</li>
                <li>null</li>
                <li>undefined</li>
            </ul>
            <h6>Syntax :</h6>
            <pre>typeof operand</pre>
            <h6>OR</h6>
            <pre>typeof (operand)</pre>
            <h6>Examples :</h6>
            <pre>
                console.log(typeof "John")  <br />
                console.log(typeof 3.14)  <br /> 
                console.log(typeof false) <br />
                console.log(typeof [1,2,3,4] )  <br />
                console.log(typeof &#123;name:'John', age:34&#125; ) <br />
                console.log(typeof new Date() )  <br />
                console.log(typeof function () &#123; &#125;  )<br />
                console.log(typeof myCar )  <br />
                console.log(typeof null )
            </pre>
            <h6>Output :</h6>
            <pre>
                string <br />
                number <br /> 
                boolean <br />
                object <br />
                object <br />
                object <br />
                function <br />
                undefined <br />
                object
            </pre>
        </div>
    )
}

export default JSTypeof