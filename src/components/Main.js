import React from 'react'
import { Link, Route, Routes } from 'react-router-dom'
import Login from './Login'
import Register from './Register'
import './main.css'
import logo2 from '../logo2.png'
import logo4 from '../logo4.png'
import logo5 from '../logo5.png'



function Main(props) {
  const { setIsAuthenticated, setIsAdmin } = props
  setIsAuthenticated(false)
  setIsAdmin(false)
  const header = {
    backgroundColor: 'white',
    color: '#fff',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '10px 20px'
  }
  const linkStyle = {
    color: '#5e4e9b',
    textDecoration: 'none',
    marginLeft: '20px',
    fontSize: '20px'
  }

  return (
    <div>
      {/* <Link to='/login'>Login</Link>
      <Link to='/register'>Register</Link> */}
      {/* <Routes>
        <Route path='/login' element={<Login />}/>
        <Route path='/register' element={<Register />}/>
      </Routes> */}

      <header style={header}>
        <div className="logo img-fluid">  <img src={logo2} alt="Logo" /></div>
        <nav>
          <Link to='/login' style={linkStyle}>Sign In</Link>
          <Link to='/register' style={linkStyle}>Register</Link>
        </nav>
      </header>
      <main className='main'>
        <div className="center-content">
        <h1 style={{fontFamily: " 'Crimson', serif, sans-serif",fontStyle: 'italic'}}>Innovate, code, and inspire.</h1>
        <h3 style={{fontFamily: " 'Crimson', serif, sans-serif",fontStyle: 'italic',paddingLeft:'50px'}}> Resources for Developers</h3>
        <p>(Documenting various technologies like JavaScript, React, Java, SQL etc...)</p>
        <button id='button'><Link to='/login' style={{color:'#5e4e9b',textDecoration:'none'}}>Get Started</Link></button>
        </div>
      </main>
    </div>
  )
}

export default Main