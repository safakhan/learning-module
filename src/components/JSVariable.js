import React from 'react'

function JSVariable() {
    
  return (
    <div> 
        <div>
                    <h3>What are Variables in JavaScript?</h3>
                    <p>Variable in JavaScript are containers for storing data.</p>
                    <h5>There are two types of variables in JavaScript: </h5>
                    <p>1.<strong> Local variables:</strong> Declare a variable inside of a block or function.   </p>
                    <p>2.<strong> Global variables:</strong> Declare a variable outside function or with a window object.</p>
                    <h6>Ways to declare a variable :</h6>
                    <ul>
                        <li> <strong>Using var  : </strong>The var is the oldest keyword to declare a variable in JavaScript. It is <strong>Global or function scoped. </strong>  </li>

                        <li> <strong>Using let : </strong>The let keyword is an improved version of the var keyword. It is <strong>Block scoped. </strong> </li>

                        <li> <strong>Using const : </strong>The const keyword has all the properties that are the same as the let keyword, except the user cannot update it.  It is <strong>Block scoped. </strong></li>

                    </ul>
                    <p><u>Example using var :</u></p>
                    <pre>
                        var x = 5;<br />
                        var y = 10;<br />
                        var z = x + y;<br />
                    </pre>
                    <p><u>Example using let :</u></p>
                    <pre>
                        let x = 5;<br />
                        let y = 10;<br />
                        let z = x + y;<br />
                    </pre>
                    <p><u>Example using const :</u></p>
                    <pre>
                        const x = 5;<br />
                        const y = 10;<br />
                        const z = x + y;<br />
                    </pre>
                </div>
    </div>
  )
}

export default JSVariable