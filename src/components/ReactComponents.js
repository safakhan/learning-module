import React from 'react'

function ReactComponents() {
    return (
        <div>
            <h2>React Components</h2>
            <ul>
                <li>React components are the <strong>building blocks</strong>  of web applications created using the React library. </li>
                <li>They are <strong> reusable, self-contained</strong> units of code that <strong>represent a specific part of a user interface</strong>.</li>
            </ul>
            <h5>There are two main types of React components:</h5>
            <ol>
                <li><strong>Functional Components: </strong>These are defined as JavaScript functions. They <strong>return JSX elements</strong> , which describe what should be rendered on the screen. They take some input data (called "props") </li><br />
                <pre>
                    <code>

                        function MyFunctionalComponent() &#123;
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;return &lt;div&gt;&#123;Hi, I am a John!&#125;&lt;/div&gt;;
                        <br />
                        &#125;

                    </code>
                </pre>
                <li><strong>Class Components: </strong>These are JavaScript classes that <strong>extend</strong>  the <strong>React.Component class</strong> . They have more advanced features like local state and lifecycle methods. They have a <strong>render()</strong> method.</li><br />
                <pre>
                    <code>
                        class MyClassComponent extends React.Component &#123;
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;constructor(props) &#123;
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;super(props);
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;this.state = &#123; count: 0 &#125;;
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&#125;
                        <br />
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;render() &#123;
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return &lt;div&gt;&#123;this.state.count&#125;&lt;/div&gt;;
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&#125;
                    </code>
                </pre>

            </ol>
        </div>
    )
}

export default ReactComponents