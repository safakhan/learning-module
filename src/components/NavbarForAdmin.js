import React from 'react'
import { Link, Route, Routes } from 'react-router-dom'
import logo2 from '../logo2.png'
import logo3 from '../logo3.png'
import logo5 from '../logo5.png'
import JavaScript from './JavaScript'
import Java from './Java';
import ReactLearning from './React';
import Home from './Home'
import Admin from './Admin'
import HTML from './HTML'


function NavbarForAdmin() {
  const reactStyle = {
    fontWeight: 'bolder',

    marginLeft: '30px',
    fontSize: '35px',
    textDecoration: 'none'
  }
  const linkStyle = {
    margin: '20px',
    color: 'white',
    fontSize: '20px',
  }
  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark custom_navbar fixed-top">
        <Link style={reactStyle} to="/home">
          <img src={logo2} alt="Logo" />
        </Link>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
              <Link style={linkStyle} to="/adminnav/JavaScript" className="javascript-link">JavaScript</Link>
            </li>
            <li className="nav-item">
              <Link style={linkStyle} to="/adminnav/react">React</Link>
            </li>

            <li className="nav-item">
              <Link style={linkStyle} to="/adminnav/java">Java</Link>
            </li>
            <li className="nav-item">
              <Link style={linkStyle} to="/adminnav/html">HTML</Link>
            </li>
            <li className="nav-item">
              <Link style={linkStyle} to="/adminnav/admin"><i class="fa-solid fa-user"></i></Link>
            </li>
            <li className="nav-item">
              <Link style={linkStyle} to="/"><i style={{ paddingBottom: '5px' }} class="fa-solid fa-right-from-bracket"></i></Link>
            </li>
          </ul>
        </div>
      </nav>
      <Routes>
      <Route path='/home' element={<Home/>}/>
         <Route path='/admin' element={<Admin/>}/>
        <Route path="/JavaScript/*" element={<JavaScript />} />
        <Route path="/java" element={<Java />} />
         <Route path="/html/*" element={<HTML />} />
          <Route path="/react/*" element={<ReactLearning />} />
      </Routes>
      
    </div>
  )
}

export default NavbarForAdmin