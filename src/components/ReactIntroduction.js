import React from 'react'
import dom from '../DOM.png';
import virtualdom from '../virtualdom.png'
function ReactIntroduction() {
    const imgStyle = {
        display:'flex',
        justifyContent:'flex-start',
        alignItems:'center',
    }
    return (
        <div>
            <div>
                <h2> <strong>Introduction to React</strong> <br /> <br /> </h2>
                <h3>  What is React ?<br /> </h3>
                <ul>
                    <li>React is an <strong> open-source JavaScript library</strong> developed by Facebook for building user interfaces.</li>
                    <li>It allows you to create <strong> reusable UI components</strong>.</li>
                    <li>React uses a <strong> virtual DOM (Document Object Model) to optimize the performance of updating the user interface</strong>, which makes it one of the fastest libraries for building user interfaces.</li>
                 </ul>
                 <h4>DOM :</h4>
                 <ul>
                    <li>The DOM is a <strong>programming interface</strong>  provided by browsers that <strong>represents</strong>  the structure of an HTML or XML document as a <strong> tree-like structure </strong>  of objects.</li>
                    <li><strong>It is a structured representation of the HTML elements.</strong></li>
                    <li>The DOM <strong>provides a way for JavaScript to interact with and manipulate the content</strong>, structure, and style of a web page <strong>dynamically</strong>.</li>
                 </ul>
                 <div  style={{display:'flex',justifyContent:'flex-start',alignItems:'center'}}>
                 <img src={dom} alt="" style={{width:'600px',height:'300px'}} />
                 </div>
                 <h5>Disadvantages of DOM :</h5>
                 <ul>
                    <li>Every time DOM gets updated, the <strong> updated element and its children have to be rendered again </strong> to update the UI of our page.</li>
                    <li>For this, <strong> each time there is a component update</strong>, the DOM needs to be updated and <strong>the UI components have to be re-rendered.</strong> </li>
                 </ul>
                 <h4>How does React Work?</h4>
                 <ul>
                 <li> Instead of manipulating the browser's DOM directly, <strong> React creates a virtual DOM in memory </strong>, where it does all the necessary manipulating, before making the changes in the browser DOM.</li>
                 <li>React finds out what changes have been made, and <strong> changes only what needs to be changed</strong>.</li>
                 </ul>
                 <h4>Virtual DOM :</h4>
                 <ul>
                    <li>React uses Virtual DOM exists which is like a lightweight <strong> copy of the actual DOM</strong></li>
                    <li>It is exactly the same, but it <strong>does not have the power to directly change the layout of the document.</strong> </li>
                    <li>Manipulating DOM is slow, but <strong> manipulating Virtual DOM is fast.</strong></li>
                    <li>Each time there is a <strong> change in the state </strong>of our application,<strong> the virtual DOM gets updated first instead of the real DOM.</strong> </li>
                 </ul>
                 <div  style={imgStyle}>
                 <img src={virtualdom} alt="" style={{width:'600px',height:'300px'}} />
                 </div>
            </div>
        </div>
    )
}

export default ReactIntroduction