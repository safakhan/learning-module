// Inside JavaScript.js
import { Route, Routes } from 'react-router-dom';
import QuickLinksForReact from './QuickLinksForReact';
import ReactClassComp from './ReactClassComp';
import ReactComponents from './ReactComponents';
import ReactIntroduction from './ReactIntroduction';
import SetUp from './SetUp';
 
function ReactLearning() {
  return (
    <div className="contentContainer">
      <QuickLinksForReact/>
      <div className="content">
        <Routes>
          <Route path='/' element = { <ReactIntroduction/> } />
          <Route path='/setup' element = { <SetUp /> } />
          <Route path='/components' element = {<ReactComponents/>}/>
          <Route path='/classcomponents' element = {< ReactClassComp/>} />
        </Routes>
         
      </div>
    </div>
  );
}

export default ReactLearning;
