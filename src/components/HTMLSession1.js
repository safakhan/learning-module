import React from 'react'

function HTMLSession1() {
  return (
    <div>
      <div>
        <h4><strong>1. Introduction to Client-Server</strong> <br /> </h4>
        <p> <strong> Definition: </strong>The client-server model is a <strong> computing architecture that
          separates</strong> the <strong>client</strong> (user interface) <strong>and the server</strong> (data storage and
          processing) components.</p>
        <p> <strong> Functionality: </strong>Clients request services or resources, and servers provide
          those services or resources.</p>
        <p> <strong>Example:</strong> In web development, the client is the user&#39;s browser, and the
          server is where the website is hosted.</p>

        <h4><strong>2. Roles of HTML, CSS, JavaScript in Web:</strong></h4>
        <p> <strong> HTML (HyperText Markup Language):</strong> Defines the <strong>structure and content</strong>  of a web page.</p>
        <p><strong>CSS (Cascading Style Sheets):</strong><strong>  Controls the presentation </strong> and layout of HTML elements.</p>
        <p><strong>JavaScript:</strong>  Adds interactivity,<strong> dynamic content</strong> , and enhances user experience.</p>
        <p><strong>Collaboration: </strong>HTML for structure, CSS for styling, and JavaScript for behavior work together to create a complete web experience.</p>

        <h4><strong>3. Basic HTML Code in Notepad and Run in Browser</strong></h4>
        <p><strong>Setting Up:</strong> Open Notepad or any text editor.</p>
        <p><strong>Basic HTML Structure:</strong> Begin by declaring the document type and opening the HTML document.</p>
        <pre>
          <code>
            &lt;!DOCTYPE html&gt;<br />
            &lt;html&gt;<br />
            &lt;head&gt;<br />
            &nbsp;&nbsp;&nbsp;&nbsp;&lt;title&gt;My First HTML Page&lt;/title&gt;<br />
            &lt;/head&gt;<br />
            &lt;body&gt;<br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;!-- Content goes here --&gt;<br />
            &lt;/body&gt;<br />
            &lt;/html&gt;
          </code>
        </pre>
        <h5><strong>Tags</strong></h5>
        <ul>
          <li><strong>&lt;!DOCTYPE html&gt;:</strong>This declaration defines the document type and version of HTML.</li>
          <li><strong>&lt;html&gt;:</strong>The root element of an HTML page.</li>
          <li><strong> &lt;head&gt;: </strong>Contains meta-information about the HTML document, such as the title.</li>
          <li><strong>&lt;title&gt;:</strong> Sets the title of the HTML document, which appears in the browser tab.</li>
          <li><strong>&lt;body&gt;:</strong> Contains the content of the HTML document.</li>
        </ul>

        <p><strong>Adding Basic HTML Elements:</strong> Inside the &lt;body&gt; section, add some basic HTML elements.</p>
        <pre>
          <code>
            &lt;!DOCTYPE html&gt;<br />
            &lt;html&gt;<br />
            &lt;head&gt;<br />
            &nbsp;&nbsp;&lt;title&gt;First Page&lt;/title&gt;<br />
            &lt;/head&gt;<br />
            &lt;body&gt;<br />
            &nbsp;&nbsp;&nbsp;&lt;h1&gt;Hello, World!&lt;/h1&gt;<br />
            &nbsp;&nbsp;&nbsp;&lt;p&gt;This is a simple HTML page.&lt;/p&gt;<br />
            &nbsp;&nbsp;&nbsp;&lt;a href="https://www.example.com"&gt;Visit Example.com&lt;/a&gt;<br />
            &nbsp;&nbsp;&nbsp;&lt;img src="example.jpg" alt="Example Image"&gt;<br />
            &lt;/body&gt;<br />
            &lt;/html&gt;

          </code>
        </pre>
        <ul>
          <li><strong>&lt;h1&gt;: </strong>Heading 1.</li>
          <li><strong>&lt;p&gt;:</strong> Paragraph.</li>
          <li><strong>&lt;a&gt;:</strong> Anchor (used for links).</li>
          <li><strong> &lt;img&gt;:</strong> Image.</li>
        </ul>
        <p><strong>Save and Run:</strong> Save the file with an .html extension, for example, index.html. Open the file in a web browser (e.g., Chrome, Firefox) by double-clicking on it or using the browser's "Open File" option.</p>
            
        <h4><strong>4. Explain Difference Between HTML 4 and 5 Versions:</strong></h4>   <br /> 
        <p><strong>HTML 4:</strong></p>
        <p>Introduced in 1997.</p>
        <p>Limited support for dynamic content and multimedia.</p>
        <p>Relied heavily on plugins for advanced features.</p>
        <p><strong>HTML 5:</strong></p>
         <p>Introduced in 2014.</p>
         <p>Designed to meet the needs of modern web applications.</p>
         <p>Supports multimedia elements, offline web apps, and improved semantics.</p>
         <p>Provides APIs for storage, communication, and more.</p>
         <p><strong>Key Points :</strong></p>
         <p>HTML5 is more efficient, supports a wider range of devices, and has enhanced semantics.</p>
         <p>HTML5 has native support for audio and video, reducing the reliance on third-party plugins</p>
      </div>
    </div>
  )
}

export default HTMLSession1