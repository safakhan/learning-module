import React from 'react'

function ReactClassComp() {
    return (
        <div>
            <h2>Class Components</h2> <br />
            <h6>Create a Class Component and Render the component in the App.js</h6>
            <pre>
                <code>
                    import React from 'react';
                    <br /><br />
                    class Classcomponent extends React.Component &#123;
                    <br />
                    &nbsp;&nbsp;render() &#123;
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;return (
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&lt;div&gt;
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;h1&gt;hello class component&lt;/h1&gt;
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&lt;/div&gt;
                    <br />
                    &nbsp;&nbsp;&#125;
                    <br />
                    &#125;
                </code>
            </pre>
            <h6>Import in the App.js</h6>
            <pre>
                <code>
                    export default Classcomponent
                    <br />
                    <br />
                    import './App.css';
                    <br />
                    import Classcomponent from './Components/Classcomponent';
                    <br />
                    <br />
                    function App() &#123;
                    <br />
                    &nbsp;&nbsp;return (
                    <br />
                    &nbsp;&nbsp;&lt;div className="App"&gt;
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&lt;h1&gt;Welcome to React Learning&lt;/h1&gt;
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&lt;h1&gt;Welcome to batch 57&lt;/h1&gt;
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&lt;Classcomponent /&gt;
                    <br />
                    &nbsp;&nbsp;&lt;/div&gt;
                    <br />
                    &#125;
                    <br />
                    export default App;
                </code>
            </pre>
            <h5>Constructor</h5>
            <ul>
                <li>A constructor is a special method in a JavaScript class that <strong> gets called when an instance of the class is created</strong>. It is used to <strong>initialize the object's properties</strong> and set up any <strong>initial values</strong>.
                </li>
                <li>In the context of a React class component, the constructor is often used to <strong> initialize the component's state</strong> and bind event handler methods. It is <strong> called with the super(props) statement</strong>, which is necessary to call the constructor of the parent class (in this case, React.Component).
                </li>
            </ul>
            <h5>State</h5>
            <ul>
                <li>In React, "state" refers to a JavaScript object that <strong>represents the data</strong> that a component needs to keep track of. It is used to <strong>store information that can change over time</strong>  and should <strong> trigger the component to re-render</strong>  when it does.</li>
            </ul>
            <h5>this Keyword</h5>
            <ul>
                <li>In JavaScript, <strong>this</strong> is a special keyword that <strong> refers to the current object</strong>  or context in which it is used.
                </li>
                <li>In the context of a class component in React, this is used to refer to the instance of the component itself. It allows you to access the component's properties, methods, and state.
                </li>
            </ul>
            <h6>Implementing Constructor, state, Access the state in the component</h6>
            <pre>
  <code>
    import React from 'react';
    <br />
    class Classcomponent extends React.Component &#123;
    <br />
    &nbsp;&nbsp;constructor(props) &#123;
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;super(props);
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;this.state = &#123;
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;count: 0
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&#125;
    <br />
    &nbsp;&nbsp;&#125;
    <br />
    <br />
    &nbsp;&nbsp;handleIncrement = () =&gt; &#123;
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;this.setState(&#123; count: this.state.count + 1 &#125;);
    <br />
    &nbsp;&nbsp;&#125;
    <br />
    <br />
    &nbsp;&nbsp;handleDecrement = () =&gt; &#123;
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;this.setState(&#123; count: this.state.count - 1 &#125;);
    <br />
    &nbsp;&nbsp;&#125;
    <br />
    <br />
    &nbsp;&nbsp;handleReset = () =&gt; &#123;
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;this.setState(&#123; count: 0 &#125;);
    <br />
    &nbsp;&nbsp;&#125;
    <br />
    <br />
    &nbsp;&nbsp;render() &#123;
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;return (
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&lt;div&gt;
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;h1&gt;hello class component&lt;/h1&gt;
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;h1&gt;&#123;this.state.count&#125;&lt;/h1&gt;
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;button onClick=&#123;this.handleIncrement&#125;&gt;Increment&lt;/button&gt;
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;button onClick=&#123;this.handleDecrement&#125;&gt;Decrement&lt;/button&gt;
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;button onClick=&#123;this.handleReset&#125;&gt;Reset&lt;/button&gt;
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&lt;/div&gt;
    <br />
    &nbsp;&nbsp;&#125;
    <br />
    &#125;
  </code>
</pre>


        </div>
    )
}

export default ReactClassComp