import React from 'react'

function AddingJS() {

    return (
        <div>
            <div>
                <h3>How to add JavaScript to HTML Document?<br /> </h3>
                <p>There are several ways to add JavaScript to an HTML document:</p>
                <p>1. <strong>Internal Script:</strong> You can include JavaScript directly within the HTML document using the &lt;script&gt; tag within &lt;head&gt; or &lt;body&gt; section. <br />
                    <u>Here's an example:</u></p>
                <pre>
                    &nbsp;&nbsp;&nbsp;&nbsp;&lt;!DOCTYPE html&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&lt;html&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;head&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;title&gt;Inline JavaScript&lt;/title&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/head&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;body&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;script&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// JavaScript code goes here<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert("Hello, world!");<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/script&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&lt;/body&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&lt;/html&gt;<br />
                </pre>


                <p>2. <strong>External JavaScript File:</strong> You can create a separate .js file containing your JavaScript code and include it in your HTML document using the &lt;script&gt;  tag with a src attribute. This is a common practice for organizing and reusing code. <br />
                    <br />  <u>Here's an example:</u></p>
                <pre>
                    &nbsp;&nbsp;&nbsp;&nbsp;&lt;!DOCTYPE html&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&lt;html&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;head&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;title&gt;External JavaScript&lt;/title&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/head&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;body&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;script src="script.js"&gt;&lt;/script&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/body&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&lt;/html&gt;<br />
                </pre>


            </div>
        </div>
    )
}

export default AddingJS