import React from 'react'

function JSFunctions() {
     
  return (
    <div> 
         <div>
                    <h2>JavaScript Functions<br /></h2><br />
                    <p>A function is a <strong>set of statements</strong> that take inputs, do some specific computation, and produce output.</p>
                    <p>Functions allow you to <strong>reuse</strong> the code</p>
                    <p> It allows us to use the same code with different arguments, to produce different results.</p><br />
                    <h4>JavaScript Function Syntax</h4><br />
                    <pre>
                        <code>
                            function functionname(parameter1, parameter2, parameter3) &#123; <br />
                            
                            &nbsp;&nbsp;&nbsp;// code to be executed <br/>
                            &#125;
                        </code>
                    </pre>
                    <p>A function is defined with the <strong>function</strong> keyword, followed by a <strong>name</strong>, followed by parentheses <strong>()</strong>.</p>
                    <p>The parentheses may include parameter names separated by commas: <strong>(parameter1, parameter2, ...)</strong></p>
                    <p>The code to be executed, by the function, is placed inside curly brackets: <strong>&#123;&#125;</strong></p>
                    <p>JavaScript Functions consists of a <strong>return </strong>statement.</p>
                    <p>When JavaScript reaches a <strong>return</strong> statement, the function will stop executing.</p> 
                    <p><strong>Parameters :</strong> Function parameters are listed inside the parentheses () in the function definition.</p>
                    <p><strong>Arguments :</strong> Function arguments are the values received by the function when it is invoked.</p>
                    <br/>
                    <h4>Invoking a JavaScript Function</h4>
                  
                    <p>In JavaScript, you can call a function by using its name followed by parentheses <strong>()</strong>. If the function expects any arguments, you pass them within the parentheses. </p>
                  
                    <pre>
                    function functionName(arg1, arg2, ...) &#123;<br />
                    &nbsp;&nbsp;&nbsp;// Function body with code <br/>
                     &#125; <br />

                    // Calling the function with arguments <br/>
                        functionName(argument1, argument2, ...);<br />
                    </pre>
                    <br/>
                    <p><u>Example 1 :</u></p>
                    <pre>
                        <code>
                            function add(number1, number2) &#123;<br />
                            &nbsp;&nbsp;&nbsp;return number1 + number2; <br />
                            &#125;<br />
                            console.log(add(6,9));
                        </code>
                    </pre>
                    <p><u> Output :</u></p>
                    <pre>15</pre>  <br/>
                    <p><u>Example 2 :</u></p>
                    <pre>
                        <code>
                            const square = function (number)  &#123;<br />
                            &nbsp;&nbsp;&nbsp; return number * number;<br />
                            &#125;;<br />
                            const num = square(4);<br />
                            console.log(num);<br />
                        </code>
                    </pre>
                    <p><u> Output :</u></p>
                    <pre>16</pre>



                </div>
    </div>
  )
}

export default JSFunctions