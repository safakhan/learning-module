import React from 'react'

function JSStringMethods() {
    return (
        <div>
            <h2>JavaScript String Methods</h2>
            <ul>
                <li><strong>String.length Property</strong></li>
                <li><strong>String.slice() Method</strong></li>
                <li><strong>String.substring() Method</strong></li>
                <li><strong>String.substr() Method</strong></li>
                <li><strong>String.replace() Method</strong></li>
                <li><strong>String.replaceAll() Method</strong></li>
                <li><strong>String.toUpperCase() Method</strong></li>
                <li><strong>String.toLowerCase() Method</strong></li>
                <li><strong>String.concat() Method</strong></li>
                <li><strong>String.trim() Method</strong></li>
                <li><strong>String.charAt() Method</strong></li>
                <li><strong>String.split() Method</strong></li> 
            </ul>
            <h5>1. Finding the length of a String</h5>
            <p> Length of a string can be determined using the JavaScript built-in <strong>length property</strong> .</p>
            <h6>Example</h6>
            <pre>
                <code>
          // Declare a string<br />
                    const str = "Talentsprint";<br /><br />

          // Display the length of String<br />
                    console.log("String Length: " + str.length);<br />
                </code>
            </pre>
            <h6>Output</h6>
            <pre>
                String Length: 12
            </pre>

            <h5>2. JavaScript String.slice() Method </h5>
            <p>It takes 2 parameters <strong>startIndex, endIndex</strong> (end not included).</p>
            <h6>Syntax</h6>
            <pre>String.slice(startIndex, endIndex)</pre>
            <h6>Example</h6>
            <pre>
                <code>
                    let A = 'Java Full Stack';<br />
                    b = A.slice(0, 4);<br />
                    c = A.slice(5, 9);<br />
                    d = A.slice(10);<br />

                    console.log(b);<br />
                    console.log(c);<br />
                    console.log(d);<br />
                </code>
            </pre>
            <h6>Output</h6>
            <pre>
                Java <br />
                Full <br />
                Stack
            </pre><br />
            <h5>3. JavaScript String.substring() Method</h5>
            <p>This method returns the part of the given string from the start index to the end index.</p>
            <h6>Syntax</h6>
            <pre>String.substring(startIndex, endIndex)</pre>
            <h6>Example</h6>
            <pre>
                <code>
                    let str = "Mind, Power, Soul";<br />
                    let part = str.substring(6, 11);<br />
                    console.log(part);<br />
                </code>
            </pre>
            <h6>Output</h6>
            <pre>Power</pre> <br />
            <h5>4. Javascript String.substr() Method </h5>
            <p>This method returns the specified number of characters from the specified index from the given string.</p>
            <h6>Syntax</h6>
            <pre>String.substr(start, length)</pre>
            <h6>Example</h6>
            <pre>
                <code>
                    let str = "Mind, Power, Soul";<br />
                    let part = str.substring(6, 5);<br />
                    console.log(part);<br />
                </code>
            </pre>
            <h6>Output</h6>
            <pre>Power</pre> <br />
            <h5>5. JavaScript String.replace() Method</h5>
            <p>This method replaces a part of the given string with another string.</p>
            <h6>Syntax</h6>
            <pre>Stringreplace(replaceValue, replaceWithValue)</pre>
            <h6>Example</h6>
            <pre>
                <code>
                    let str = "Mind, Power, Soul";<br />
                    let part = str.replace("Power", "Space");<br />
                    console.log(part);<br />
                </code>
            </pre>
            <h6>Output</h6>
            <pre>Mind, Space, Soul</pre>
            <h5>6. JavaScript String.replaceAll() Method</h5>
            <h6>Syntax</h6>
            <pre>String.replaceAll(substr, newSubstr)</pre>
            <h6>Example</h6>
            <pre>
                <code>
                    let str = "Mind, Power, Power, Soul";<br />
                    let part = str.replaceAll("Power", "Space");<br />
                    console.log(part);<br />
                </code>
            </pre>
            <h6>Output</h6>
            <pre>Mind, Space, Space,  Soul</pre> <br />
            <h5>7. JavaScript String.toUpperCase() Method</h5>
            <p>This method converts all the characters present in the String to upper case.</p>
            <h6>Syntax</h6>
            <pre>str.toUpperCase()</pre>
            <h6>Example</h6>
            <pre>
                <code> 
                    let str = 'talentsprint'; <br/>

                    console.log(str.toUpperCase(str));
                </code>
            </pre>
            <h6>Output</h6>
            <pre>TALENTSPRINT</pre> <br />
            <h5>8. JavaScript String.toLowerCase() Method</h5>
            <p>This method converts all the characters present in the so lowercase.</p>
            <h6>Syntax</h6>
            <pre>str.toLowerCase()</pre>
            <h6>Example</h6>
            <pre>
                let str = 'Java Full Stack' <br />
                console.log(str.toLowerCase(str));
            </pre>
            <h6>Output</h6>
            <pre>java full stack</pre> <br />
            <h5>9. JavaScript String.concat() Method</h5>
            <p>This method combines the text of two strings and returns a new combined or joined string.</p>
            <h6>Syntax</h6>
            <pre>String1.concat(String2)</pre>
            <h6>Example</h6>
            <pre>
                let str1 = 'Java' <br />
                let str2 = 'Full Stack Development' <br />
                console.log(str1.concat(str2))
            </pre>
            <h6>Output</h6>
            <pre>Java Full Stack Development</pre> <br />
            <h5>10. JavaScript String.trim() Method</h5>
            <p>This method is used to remove either white spaces from the given string.</p>
            <h6>Syntax</h6>
            <pre>String.trim()</pre>
            <h6>Example</h6>
            <pre>
                let name = 'John   '<br />
                let len = name.length <br />
                console.log(len); <br />
                let newName = name.trim() <br />
                console.log(newName.length)
            </pre>
            <h6>Output</h6>
            <pre>
                7 <br />
                4
            </pre> <br />
            <h5>11. JavaScript String.charAt() Method</h5>
            <p>This method returns the character at the specified index.</p>
            <h6>Syntax</h6>
            <pre>String.charAt(indexOfChar)</pre>
            <h6>Example</h6>
            <pre>
                let str = "JavaScript" <br /><br />
                console.log(str.charAt(0)); <br />
                console.log(str.charAt(5))
            </pre>
            <h6>Output</h6>
            <pre>
                J <br />
                c
            </pre> <br />
            <h5>12. JavaScript String.split()</h5>
            <p>This method splits the string into an array of sub-strings and returns an array. </p>
            <h6>Syntax</h6>
            <pre>String,split(character)</pre>
            <h6>Example</h6>
            <pre>
                let str = "Full Stack Development" <br /> <br />
                console.log(str.split(' '));
            </pre>
            <h6>Output</h6>
            <pre>
                ['Full', 'Stack', 'Development']
            </pre> 
        </div>
    )
}

export default JSStringMethods