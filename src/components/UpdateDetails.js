import React from 'react'
import Update from './Update';
import axios from 'axios';
import { useState } from 'react';
import { useEffect } from 'react';

function UpdateDetails(props) {
    const { profileupdate, setIsAuthenticated } = props
    const [details, setDetails] = useState(profileupdate)
    const divStyle = {
        boxSizing: 'border-box',
        margin: '20px',
        marginTop: '70px'
    };
    const tableStyle = {
        borderCollapse: 'collapse',
        width: '100%',
    };

    const rowStyle = {
        borderBottom: '1px solid #dddddd',
    };

    const cellStyle = {
        border: '1px solid #dddddd',
        textAlign: 'left',
        padding: '8px',
    };
    //    async function fetchdata(id){
    //         try{
    //             console.log("kjdbvvb");
    //             let response = await axios.get(`http://localhost:8000/getdata/${id}`)
    //              setDetails(response.data)
    //              setIsAuthenticated(true)
    //         }catch(error){
    //             console.log(error)
    //         }
    //     }
    const updatingData = async (formdata, id) => {
        // event.preventDefault();
        console.log(formdata)
        console.log(id)
        try {
            const response = await axios.put(`http://localhost:8000/putdata/${id}`, formdata)
            //  fetchdata(id)
        } catch (error) {
            console.log(error)
        }
    }

    return (
        <div>
            <div style={divStyle}>
                <table style={tableStyle}>
                    <thead>
                        <tr style={rowStyle}>
                            <th style={cellStyle}>First Name</th>
                            <th style={cellStyle}>Last Name</th>
                            <th style={cellStyle}>Gender</th>
                            <th style={cellStyle}>Email</th>
                            <th style={cellStyle}>Phone Number</th>
                            <th style={cellStyle}>Country</th>
                            <th style={cellStyle}>City</th>
                            <th style={cellStyle}>Update</th>


                        </tr>

                    </thead>
                    <tbody>
                        <tr style={rowStyle} key={details._id}>
                            <td style={cellStyle}>{details.fname}</td>
                            <td style={cellStyle}>{details.lname}</td>
                            <td style={cellStyle}>{details.gender}</td>
                            <td style={cellStyle}>{details.email}</td>
                            <td style={cellStyle}>{details.phone}</td>
                            <td style={cellStyle}>{details.country}</td>
                            <td style={cellStyle}>{details.city}</td>
                            {/* <td style={cellStyle} ><Update updatingData={(formdata) => updatingData(formdata, details._id)} /> */}
                            <td style={cellStyle} ><Update updatingData={updatingData} id={details} />

                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

    )
}

export default UpdateDetails