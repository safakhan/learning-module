import React from 'react'

function JSStrings() {
  return (
    <div>
      <h2>JavaScript Strings</h2>
      <p>JavaScript String Object is a <strong>sequence of characters</strong>. It contains zero or more characters within single or double quotes.</p>
      <h6>Syntax</h6>
      <pre>
        const string_name = "String Content" <br />
        or <br />
        const string_name = new String("String Content")
        <h6>Example</h6>
      </pre>
      <pre>
        let text = "NSE Talentsprint"; <br />
        console.log(text); <br />
        let str = new String("Second String") <br />
        console.log(str)
      </pre>
      <h6>Output</h6>
      <pre>
        NSE Talentsprint <br />
        Second String
      </pre>    <br />
      

    </div>
  )
}

export default JSStrings