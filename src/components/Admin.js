import axios from 'axios';
import React, { useEffect,useState } from 'react'
import Update from './Update';

function Admin(props) {
    const {setIsAdmin} = props
    const [details,setDetails] = useState([])

    const divStyle = {
        boxSizing: 'border-box',  
        margin:'20px',
        marginTop:'70px'
      };
    const tableStyle = {
        borderCollapse: 'collapse',
        width: '100%',
    };

    const rowStyle = {
        borderBottom: '1px solid #dddddd',
    };

    const cellStyle = {
        border: '1px solid #dddddd',
        textAlign: 'left',
        padding: '8px',
    };
    async function fetchdata(){
        try{
            let response = await axios.get('http://localhost:8000/fetch')
            setDetails(response.data)
            setIsAdmin(true)
        }catch(error){
            console.log(error)
        }

    }

    async function handleDelete(id) {
        console.log("khdjy");
        try {
            await axios.delete(`http://localhost:8000/deletedata/${id}`)
            fetchdata();

        } catch (error) {
            console.log(error)
        }
    }

    const updatingData = async (formdata,id)=>{
        console.log(formdata)
        // event.preventDefault()
        try{
            const response = await axios.put(`http://localhost:8000/putdata/${id}`,formdata)
            // fetchdata()
        }catch(error){
            console.log(error)
        }
    }

    useEffect(()=>{
        console.log("sydgkasc");
        fetchdata()
    },[])
    return (
        <div>
            <div style={divStyle}>
                <table style={tableStyle}>
                    <thead>
                        <tr style={rowStyle}>
                            <th style={cellStyle}>First Name</th>
                            <th style={cellStyle}>Last Name</th>
                            <th style={cellStyle}>Gender</th>
                            <th style={cellStyle}>Email</th>
                            <th style={cellStyle}>Phone Number</th>
                            <th style={cellStyle}>Country</th>
                            <th style={cellStyle}>City</th>
                            <th style={cellStyle}>Delete</th>
                            <th style={cellStyle}>Update</th>


                        </tr>
                        
                    </thead>
                    <tbody>
                        {
                        details.map((item)=>
                        <tr style={rowStyle} key={item._id}>
                            <td style={cellStyle}>{item.fname}</td>
                            <td style={cellStyle}>{item.lname}</td>
                            <td style={cellStyle}>{item.gender}</td>
                            <td style={cellStyle}>{item.email}</td>
                            <td style={cellStyle}>{item.phone}</td>
                            <td style={cellStyle}>{item.country}</td>
                            <td style={cellStyle}>{item.city}</td>
                            <td style={cellStyle}><button onClick={()=>{handleDelete(item._id)}}>Delete</button></td>
                            <td style={cellStyle} > <Update updatingData={updatingData} id={item._id}/> </td>

                        </tr>
                        )}
                        
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default Admin