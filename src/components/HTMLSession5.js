import React from 'react'

function HTMLSession5() {
  return (
    <div>
      <h4><strong>Audio and Video Tags</strong></h4>
      <pre>
        <code>
          &lt;!DOCTYPE html&gt;<br />
          &lt;html lang=&quot;en&quot;&gt;<br />
          &lt;head&gt;<br />
          &nbsp;&nbsp;&lt;meta charset=&quot;UTF-8&quot;&gt;<br />
          &nbsp;&nbsp;&lt;meta http-equiv=&quot;X-UA-Compatible&quot; content=&quot;IE=edge&quot;&gt;<br />
          &nbsp;&nbsp;&lt;meta name=&quot;viewport&quot; content=&quot;width=device-width, initial-scale=1.0&quot;&gt;<br />
          &nbsp;&nbsp;&lt;title&gt;HTML Tags&lt;/title&gt;<br />
          &lt;/head&gt;<br />
          &lt;body&gt;<br />
          &nbsp;&nbsp;&lt;a href=&quot;Day01Formtags.html&quot; target=&quot;_blank&quot;&gt;Day01 Form&lt;/a&gt;<br />
          &nbsp;&nbsp;&lt;video controls height=&quot;400px&quot; width=&quot;400px&quot; muted&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;source src=&quot;audio and vedeo/LookAtMe.mp4&quot;&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;source src=&quot;audio and vedeo/LookAtMe.mp4&quot;&gt;<br />
          &nbsp;&nbsp;&lt;/video&gt;<br />
          &nbsp;&nbsp;&lt;video src=&quot;audio and vedeo/LookAtMe.mp4&quot; controls height=&quot;400px&quot; width=&quot;400px&quot;<br />
          &nbsp;&nbsp;&gt;<br />
          <br />
          &lt;/video&gt;<br />
          <br />
          &lt;audio src=&quot;audio and vedeo/CheapThrills.mp3&quot; muted controls loop&gt;&lt;/audio&gt;<br />
          <br />
          &lt;/body&gt;<br />
          &lt;/html&gt;

        </code>
      </pre>
    </div>
  )
}

export default HTMLSession5