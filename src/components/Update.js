import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {Form,FormGroup,Label,Input} from 'reactstrap'

function Update(props) {
  const [modal, setModal] = useState(false);
  const [error, setError] = useState(null); 

  const toggle = () => setModal(!modal);

  const {updatingData,id} = props

  const [formdata,setFormdata] = useState({
    fname:id.fname,
    lname:id.lname,
    gender:id.gender,
    email:id.email,
    phone:id.phone,
    country:id.country,
    city:id.city,
  })
  const handleInput = (event)=>{
      event.preventDefault()
      const {name,value} = event.target
      setFormdata({
        ...formdata,
        [name]:value
      })
  }
  const handleSubmit = () => {
    toggle();
    updatingData(formdata,id._id);
  };
  // const handleSubmit = async (event) => { 
  //   try {
  //     await updatingData(formdata, id, event);
  //     toggle();
  //   } catch (error) {
  //     setError("An error occurred. Please try again.");  
  //   }
  // };

  return (
    <div>
      <Button color="danger" onClick={toggle}>
        Click Me
      </Button>
      <Modal isOpen={modal} toggle={toggle} >
        <ModalHeader toggle={toggle}>Modal title</ModalHeader>
        <ModalBody>
          <Form onSubmit={handleSubmit}>
          <FormGroup floating>
              <Input
                id="fname"
                name="fname"
                placeholder="First Name"
                type="text"
                value={formdata.fname}
                onChange={handleInput}
              />
              <Label for="fname">
                First Name
              </Label>
            </FormGroup>
            {' '}
            <FormGroup floating>
              <Input
                id="lname"
                name="lname"
                placeholder="Last Name"
                type="text"
                value={formdata.lname}
                onChange={handleInput}
              />
              <Label for="name">
                Last Name
              </Label>
            </FormGroup>
            {' '}
            <FormGroup floating>
              <Input
                id="gender"
                name="gender"
                placeholder="Gender"
                type="text"
                value={formdata.gender}
                onChange={handleInput}
              />
              <Label for="gender">
                Gender
              </Label>
            </FormGroup>
            {' '}
            <FormGroup floating>
              <Input
                id="email"
                name="email"
                placeholder="Email"
                type="text"
                value={formdata.email}
                onChange={handleInput}
              />
              <Label for="email">
                Email
              </Label>
            </FormGroup>
            {' '}
            <FormGroup floating>
              <Input
                id="phone"
                name="phone"
                placeholder="Phone Number"
                type="text"
                value={formdata.phone}
                onChange={handleInput}
              />
              <Label for="phone">
                Phone
              </Label>
            </FormGroup>
            {' '}
            <FormGroup floating>
              <Input
                id="country"
                name="country"
                placeholder="Country"
                type="text"
                value={formdata.country}
                onChange={handleInput}
              />
              <Label for="country">
                Country
              </Label>
            </FormGroup>
            {' '}
            <FormGroup floating>
              <Input
                id="city"
                name="city"
                placeholder="City"
                type="text"
                value={formdata.city}
                onChange={handleInput}
              />
              <Label for="city">
                City
              </Label>
            </FormGroup>
            {' '}
            <Button type='submit'>
              Submit
            </Button>
          </Form>
        </ModalBody>
        {/* <ModalFooter>
          <Button color="primary" onClick={toggle}>
            Do Something
          </Button>{' '}
          <Button color="secondary" onClick={toggle}>
            Cancel
          </Button>
        </ModalFooter> */}
      </Modal>
    </div>
  );
}

export default Update;