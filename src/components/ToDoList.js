import React from 'react'

function ToDoList() {
  return (
    <div>
        <h2>To Do List</h2> <br />
        <h6>-- index.html --</h6>
        <pre>
  <code>
     
    &lt;!DOCTYPE html&gt;<br />
    &lt;html lang="en"&gt;<br />
    &lt;head&gt;<br />
        &nbsp;&nbsp;&lt;meta charset="UTF-8"&gt;<br />
        &nbsp;&nbsp;&lt;meta name="viewport" content="width=device-width, initial-scale=1.0"&gt;<br />
        &nbsp;&nbsp;&lt;title&gt;ToDoList App&lt;/title&gt;<br />
        &nbsp;&nbsp;&lt;link rel="stylesheet" href="styles.css"&gt;<br />
        &nbsp;&nbsp;&lt;!-- FontAwesome CDN --&gt;<br />
        &nbsp;&nbsp;&lt;link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@6.2.1/css/fontawesome.min.css"&gt;<br />
    &lt;/head&gt;<br />
    &lt;body&gt;<br />
        &nbsp;&nbsp;&lt;div class="app"&gt;<br />
            &nbsp;&nbsp;&nbsp;&nbsp;&lt;div class="ToDoapp_container"&gt;<br />
                &nbsp;&nbsp;&nbsp;&nbsp;&lt;div class="usertsk_container"&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;p id="errormsg"&gt;&lt;/p&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;br /&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;input type="text" id="userdata" placeholder="Enter the Task" id="usertask"&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;br /&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;button onclick="addtask()" class="addbtn"&gt;Add&lt;/button&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;br /&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ul id="task_container"&gt;<br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;!--<br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;div class="todotask"&gt;<br />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;li class="lstele"&gt;html&lt;/li&gt;<br />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;i class="fa-solid fa-delete-left"&gt;&lt;/i&gt;<br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/div&gt;<br />
                        --&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/ul&gt;<br />
                &nbsp;&nbsp;&nbsp;&nbsp;&lt;/div&gt;<br />
            &nbsp;&nbsp;&lt;/div&gt;<br />
        &nbsp;&nbsp;&lt;/div&gt;<br />
        &nbsp;&nbsp;&lt;script src="scripts.js"&gt;&lt;/script&gt;<br />
    &lt;/body&gt;<br />
    &lt;/html&gt;
  </code>
</pre><br />
<h6>-- styles.css --</h6>
<pre>
  <code>
    .ToDoapp_container &#123; <br />
        &nbsp;&nbsp;border: 1px solid black;<br />
        &nbsp;&nbsp;height: 500px;<br />
        &nbsp;&nbsp;width: 500px;<br />
        &nbsp;&nbsp;display: flex;<br />
        &nbsp;&nbsp;/* justify-content: start; */<br />
        &nbsp;&nbsp;flex-direction: column;<br />
        &nbsp;&nbsp;background-color: blue;<br />
    &#125; <br />
    
    .app &#123; <br />
        &nbsp;&nbsp;display: flex;<br />
        &nbsp;&nbsp;justify-content: center;<br />
    &#125; <br />
    
    .usertsk_container &#123; <br />
        &nbsp;&nbsp;width: 350px;<br />
        &nbsp;&nbsp;margin-left: 100px;<br />
        &nbsp;&nbsp;/* border: 1px solid black; */<br />
    &#125; <br />
    
    #userdata &#123; <br />
        &nbsp;&nbsp;margin-top: 10px;<br />
        &nbsp;&nbsp;border: 1px solid black;<br />
        &nbsp;&nbsp;border-radius: 10px;<br />
        &nbsp;&nbsp;padding: 20px;<br />
        &nbsp;&nbsp;width: 200px;<br />
        &nbsp;&nbsp;color: rgb(8, 8, 49);<br />
    &#125; <br />
    
    .addbtn &#123; <br />
        &nbsp;&nbsp;/* width: 30px; */<br />
        &nbsp;&nbsp;margin-top: 10px;<br />
        &nbsp;&nbsp;margin-left: 10px;<br />
        &nbsp;&nbsp;border-radius: 10px;<br />
        &nbsp;&nbsp;height: 45px;<br />
        &nbsp;&nbsp;width: 50px;<br />
    &#125; <br />
    
    .addbtn:hover &#123; <br />
        &nbsp;&nbsp;color: white;<br />
        &nbsp;&nbsp;background-color: blue;<br />
    &#125; <br />
    
    
    .todotask &#123; <br />
        &nbsp;&nbsp;display: flex;<br />
        &nbsp;&nbsp;flex-direction: row;<br />
        &nbsp;&nbsp;align-items: center;<br />
    &#125; <br />
    
    .lstele &#123; <br />
        &nbsp;&nbsp;width: 80px;<br />
    &#125; <br />
    
    .taskcompleted &#123; <br />
        &nbsp;&nbsp;text-decoration: line-through;<br />
    &#125;
  </code>
</pre>
<h6>-- scripts.js --</h6>
<pre>
  <code>
    let usergiventask = document.getElementById('userdata');<br />
    let taskContainer = document.getElementById('task_container');<br />
    let task = [];<br /><br />

    function addtask() &#123;<br />
    &nbsp;&nbsp;if (usergiventask.value == "") &#123;<br />
    &nbsp;&nbsp;document.getElementById('errormsg').innerHTML = "Please enter a valid task";<br />
    &nbsp;&#125;else &#123;<br />
    &nbsp;&nbsp;document.getElementById('errormsg').innerHTML = "";<br />
    &nbsp;&nbsp;task.push(usergiventask.value);<br />
    &nbsp;&nbsp;console.log(task);<br />
    &nbsp;&nbsp;usergiventask.value = "";<br />
    &nbsp;&nbsp;displayTasks(task);<br />
    &nbsp;&#125;<br />
    &#125;<br /><br />

    function displayTasks(task) &#123; <br />
    &nbsp;taskContainer.innerHTML = ""; <br />
    &nbsp;task.forEach((element, index) =&gt; &#123;<br />
    &nbsp;&nbsp;let lst = document.createElement('li'); // Creating the first li element<br />
    &nbsp;&nbsp;lst.classList.add('lstele');<br />
    &nbsp;&nbsp;lst.innerHTML = element;<br />
    &nbsp;&nbsp;lst.addEventListener('click', () =&gt; &#123;<br />
    &nbsp;&nbsp;&nbsp;lst.classList.toggle('taskcompleted');<br />
    &nbsp;&nbsp;&#125;);<br /><br />

    &nbsp;&nbsp;let icon = document.createElement('i'); // Creating an icon<br />
    &nbsp;&nbsp;icon.classList.add('fa-solid', 'fa-delete-left');<br /><br />

    &nbsp;&nbsp;icon.onclick = () =&gt; &#123;<br />
    &nbsp;&nbsp;&nbsp;task.splice(index, 1);<br />
    &nbsp;&nbsp;&nbsp;taskContainer.innerHTML = "";<br />
    &nbsp;&nbsp;&nbsp;displayTasks(task);<br />
    &nbsp;&nbsp;&#125;<br /><br />

    &nbsp;&nbsp;let divcontainer = document.createElement('div'); // Create a div container<br />
    &nbsp;&nbsp;divcontainer.classList.add('todotask');<br />
    &nbsp;&nbsp;divcontainer.appendChild(lst);<br />
    &nbsp;&nbsp;divcontainer.appendChild(icon);<br />
    &nbsp;&nbsp;taskContainer.appendChild(divcontainer);  <br />
    &nbsp;&#125;);<br />
    &#125;<br />
  </code>
</pre>


    </div>
  )
}

export default ToDoList