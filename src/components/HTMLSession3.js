import React from 'react'

function HTMLSession3() {
    return (
        <div>
            <h4><strong>1. Heading Level Tags and Paragraph Tag</strong> <br /> </h4>
            <p> Introduction to heading tags (h1 to h6) and the paragraph tag (p).</p>
            <p> Proper usage of heading tags for structuring content.</p>
            <pre>
                <code>
                    &lt;h1&gt;This is heading1&lt;/h1&gt;<br />
                    &lt;h2&gt;This is heading2&lt;/h2&gt;<br />
                    &lt;h3&gt;This is heading3&lt;/h3&gt;<br />
                    &lt;h4&gt;This is heading4&lt;/h4&gt;<br />
                    &lt;h5&gt;This is heading5&lt;/h5&gt;<br />
                    &lt;h6&gt;This is heading6&lt;/h6&gt; <br /><br />
                    &lt;p&gt;This is paragraph&lt;/p&gt;


                </code>
            </pre>
            <h4><strong>2. Font Tag and Attributes</strong> <br /> </h4>
            <p>Explanation of the font tag and its attributes. Using attributes like size, color, and face to style text.</p>
            <h6>Example:</h6>
            <strong> &lt;font size="4" color="blue" face="Arial"&gt;Styled Text&lt;/font&gt;</strong><br /><br />

            <h4><strong>3. Bold, Italic, Underline, Super, Sub Tags</strong></h4>
            <p> Implementation of text formatting with strong (bold), em    (italic), u (underline), sup (superscript), and sub (subscript)  tags. Styling text for emphasis and hierarchy.</p>
            <h6> Example:</h6>
            <pre>
                <code>
                    &lt;strong&gt;Bold Text&lt;/strong&gt;<br />
                    &lt;em&gt;Italic Text&lt;/em&gt;<br />
                    &lt;u&gt;Underlined Text&lt;/u&gt;<br />
                    &lt;sup&gt;Superscript&lt;/sup&gt;<br />
                    &lt;sub&gt;Subscript&lt;/sub&gt;


                </code>
            </pre>
            <h4><strong>4. Pre Tag</strong></h4>
            <p> Introduction to the pre (preformatted text) tag. Preserving white spaces and line breaks in the displayed text.</p>
            <h6>Example:</h6>
            <pre>
                <code>
                    &lt;pre&gt;<br />
                    &nbsp;&nbsp;This   is<br />
                    &nbsp;&nbsp;preformatted<br />
                    &nbsp;&nbsp;text.<br />
                    &lt;/pre&gt;

                </code>
            </pre>
            <h4><strong>5. Line Break and Centering Text</strong></h4>
            <p> Use of the br (line break) tag to create line breaks. Centering text within a container using the center tag (deprecated in HTML5, but useful for understanding).</p>
            <pre>
                <code>
                    Line 1&lt;br&gt;Line 2&lt;br&gt;Line 3<br />
                    &nbsp;&nbsp;&lt;center&gt;Centered Text&lt;/center&gt;
                </code>
            </pre>
            <h4><strong>6. Marquee</strong></h4>
            <p> Introduction to the marquee tag for creating scrolling text. Implementation of the marquee tag with attributes for direction, behavior, and speed.</p>
            <h6>Example:</h6>
            <pre>
                <code>
                &lt;marquee direction="left" behavior="scroll" scrollamount="5"&gt;Scrolling Text&lt;/marquee&gt;

                </code>
            </pre>
        </div>
    )
}

export default HTMLSession3