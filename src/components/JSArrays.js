import React from 'react'

function JSArrays() {
    return (
        <div>
            <h2>JavaScript Arrays</h2>
            <p>JavaScript Array is a single variable that is used to <strong>store elements of different data types</strong>.
                JavaScript arrays are zero-indexed.An array allows you to store several values with the same name and <strong>access them by using their index number</strong>.</p>
            <h5>There are two ways to declare an Array </h5><br />
            <h5>1. Creating an array using array literal:</h5>
            <pre>let arrayName = [value1, value2, ...];</pre>
            <h6>Example</h6>
            <pre>
                <code>
                    let courses = ["HTML", "CSS", "Javascript", "React"];<br />
                    console.log(courses);<br />
                    <br />
                </code>
            </pre>
            <h6>Output</h6>
            <pre>[ 'HTML', 'CSS', 'Javascript', 'React' ]</pre><br />

            <h5>2. Creating an array using the JavaScript new keyword</h5>
            <pre>let arrayName = new Array();</pre>
            <h6>Example</h6>
            <pre>const courses = new Array("HTML", "CSS", "Javascript");<br />
                console.log(courses);
            </pre>
            <h6>Output</h6>
            <pre>[ 'HTML', 'CSS', 'Javascript' ]</pre><br />
            <h5>Accessing Elements of an Array</h5>
            <p>Any element in the array can be accessed <strong>using the index number</strong>. The index in the arrays starts with 0.</p>
            <h6>Example</h6>
            <pre>
                <code>
                    const courses = ["HTML", "CSS", "Javascript"];
                     
                    <br />
                    console.log(courses);
                </code>
            </pre>
            <h6>Output</h6>
            <pre>HTML <br />
                CSS <br />
                Javascript
            </pre>
            <h5>Change elements from a pre-defined array</h5>
            <p>Use index based method method to change the elements of array.</p>
            <h6>Example</h6>
            <pre>
                <code>
                    const courses = ["HTML", "CSS", "Javascript"];
                    <br />
                    console.log(courses);
                    <br />
                    courses[1] = "React";
                    <br />
                    console.log(courses);
                    <br />
                </code>
            </pre>
            <h6>Output</h6>
            <pre>
                [ 'HTML', 'CSS', 'Javascript' ] <br />
                [ 'HTML', 'React', 'Javascript' ]
            </pre><br />
            <h5>Loop through Javascript Array Elements</h5>
            <p>We can loop through the elements of a Javascript array using the <strong> for loop</strong>:</p>
            <h6>Example</h6>
            <pre>
                <code>
                    const courses = ["HTML", "CSS", "Javascript"];
                    <br />
                    for (let i = 0; i &lt; courses.length; i++) &#123;
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;console.log(courses[i]);
                    <br />
                    &#125;
                </code>
            </pre>
            <h6>Output</h6>
            <pre>HTML <br />
                CSS <br />
                Javascript
            </pre>
            <p>This can also be done by <strong> using the Array.forEach() </strong>function of Javascript.</p>
            <pre>
                <code>
                    const courses = ["HTML", "CSS", "Javascript"];
                     courses.forEach(course =&gt; &#123;
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp; console.log(course);
                    <br />
                    &#125;);
                </code>
            </pre>
            <h6>Output</h6>
            <pre>HTML <br />
                CSS <br />
                Javascript
            </pre>




        </div>
    )
}

export default JSArrays