import React from 'react'

function HTMLSession7() {
  return (
    <div>
      <h4><strong>1. FrameSet</strong></h4>
      <p>The frameset tag (&lt;frameset&gt;) was used in HTML to create frames within a web page. However, it is now deprecated in HTML5, and the use of iframes is recommended instead.</p>
      <h6>Example :</h6>
      <pre>
        <code>
          &lt;!DOCTYPE html&gt;<br />
          &lt;html&gt;<br />
          &lt;frameset cols=&quot;25%,50%,25%&quot;&gt;<br />
          &nbsp;&nbsp;&lt;frame src=&quot;frame1.html&quot;&gt;<br />
          &nbsp;&nbsp;&lt;frame src=&quot;frame2.html&quot;&gt;<br />
          &nbsp;&nbsp;&lt;frame src=&quot;frame3.html&quot;&gt;<br />
          &lt;/frameset&gt;<br />
          &lt;/html&gt;

        </code>
      </pre>
      <h6>Explanation</h6>
      <p>&lt;frameset&gt; is used to define the frames structure. The cols attribute defines the width of each column.</p>
      <strong>Note: It&#39;s recommended to use modern alternatives like CSS forcreating layouts instead of framesets.</strong><br /><br />
      <h4><strong>2. iFrame</strong></h4>
      <p>The iframe tag (&lt;iframe&gt;) is used to embed another HTML document within the current HTML document. It is a more modern and flexible alternative to framesets.</p>
      <h6>Example</h6>
      <pre>
        <code>
          &lt;iframe src=&quot;https://www.example.com&quot; width=&quot;600&quot;
          height=&quot;400&quot; title=&quot;Example Website&quot;&gt;&lt;/iframe&gt;
        </code>
      </pre>
      <h6>Explanation:</h6>
      <li>The src attribute specifies the URL of the content to be embedded.</li>
      <li>The width and height attributes define the dimensions of the iframe.</li>
      <h6>Advanced Example (with additional attributes):</h6>
      <pre>&lt;iframe src=&quot;video.mp4&quot; width=&quot;800&quot; height=&quot;600&quot;            frameborder=&quot;0&quot; allowfullscreen&gt;&lt;/iframe&gt;</pre>
      <h6>Explanation</h6>
      <li>frameborder=&quot;0&quot; removes the border around the iframe.</li>
      <li>allowfullscreen enables fullscreen mode for embedded content.</li>

      <h4><strong>Tags of HTML and HTML5 :</strong></h4>
      <ul>
        <li>
          <strong> Heading Tags:</strong> <br />
          <p>&lt;h1&gt; to &lt;h6&gt;: Define headings of different levels.</p>
          <h6>Example :</h6>
          <pre>&lt;h1&gt;This is a Heading 1&lt;/h1&gt;<br />
            &lt;h2&gt;This is a Heading 2&lt;/h2&gt;</pre>
        </li>
        <li> <strong>Paragraph Tag</strong>
          <p>&lt;p&gt;: Defines a paragraph.</p>
          <h6>Example :</h6>
          <pre>&lt;p&gt;This is a paragraph.&lt;/p&gt;</pre>
        </li>
        <li><strong>Anchor Tag</strong></li>
        <p>&lt;a&gt;: Creates hyperlinks.</p>
        <h6>Example :</h6>
        <pre>&lt;a href=&quot;https://www.example.com&quot; target=&quot;_blank&quot;&gt;Visit Example&lt;/a&gt;</pre>
        <li><strong>Image Tag</strong>
          <p>&lt;img&gt;: Embeds images in the document.</p>
          <h6>Example :</h6>
          <pre>&lt;img src=&quot;image.jpg&quot; alt=&quot;Description of the image&quot;&gt;</pre>
        </li>
        <li><strong>List Tags (Ordered and Unordered)</strong>
          <p>&lt;ol&gt;: Creates an ordered list. <br />
            &lt;ul&gt;: Creates an unordered list. <br />
            &lt;li&gt;: Defines list items.</p>

          <h6>Example :</h6>
          <pre>
            &lt;ol&gt; <br />
            &lt;li&gt;Item 1&lt;/li&gt;<br />
            &lt;li&gt;Item 2&lt;/li&gt;<br />
            &lt;/ol&gt;<br />
            &lt;ul&gt;<br />
            &lt;li&gt;Item A&lt;/li&gt;<br />
            &lt;li&gt;Item B&lt;/li&gt;<br />
            &lt;/ul&gt;<br />
          </pre>

        </li>
        <li><strong>Table Tag</strong>
          <p>&lt;table&gt;: Creates a table. <br />
            &lt;tr&gt;: Defines a table row. <br />
            &lt;th&gt;: Defines a table header cell. <br />
            &lt;td&gt;: Defines a table data cell.</p>
          <h6>Example :</h6>
          <pre>
            &lt;table&gt;<br />
            &nbsp;&nbsp;&lt;tr&gt;<br />
            &nbsp;&nbsp;&nbsp;&nbsp;&lt;th&gt;Header 1&lt;/th&gt;<br />
            &nbsp;&nbsp;&nbsp;&nbsp;&lt;th&gt;Header 2&lt;/th&gt;<br />
            &nbsp;&nbsp;&lt;/tr&gt;<br />
            &nbsp;&nbsp;&lt;tr&gt;<br />
            &nbsp;&nbsp;&nbsp;&nbsp;&lt;td&gt;Data 1&lt;/td&gt;<br />
            &nbsp;&nbsp;&nbsp;&nbsp;&lt;td&gt;Data 2&lt;/td&gt;<br />
            &nbsp;&nbsp;&lt;/tr&gt;<br />
            &lt;/table&gt;

          </pre>
        </li>
        <li><strong>Form Tags</strong>
          <p>&lt;form&gt;: Defines an HTML form. <br />
            &lt;input&gt;: Defines an input field. <br />
            &lt;textarea&gt;: Defines a multiline text input control. <br />
            &lt;select&gt;: Defines a dropdown list.</p>
          <h6>Example :</h6>
          <pre>
            &lt;form action=&quot;/submit&quot; method=&quot;post&quot;&gt;<br />
            &nbsp;&nbsp;&lt;label for=&quot;username&quot;&gt;Username:&lt;/label&gt;<br />
            &nbsp;&nbsp;&lt;input type=&quot;text&quot; id=&quot;username&quot; name=&quot;username&quot;&gt;<br />
            &nbsp;&nbsp;&lt;textarea rows=&quot;4&quot; cols=&quot;50&quot;&gt;Enter text here...&lt;/textarea&gt;<br />
            &nbsp;&nbsp;&lt;select&gt;<br />
            &nbsp;&nbsp;&nbsp;&nbsp;&lt;option value=&quot;option1&quot;&gt;Option 1&lt;/option&gt;<br />
            &nbsp;&nbsp;&nbsp;&nbsp;&lt;option value=&quot;option2&quot;&gt;Option 2&lt;/option&gt;<br />
            &nbsp;&nbsp;&lt;/select&gt;<br />
            &nbsp;&nbsp;&lt;input type=&quot;submit&quot; value=&quot;Submit&quot;&gt;<br />
            &lt;/form&gt;

          </pre>
        </li>
        <li><strong>Semantic Tags (HTML5)</strong>
          <p>&lt;header&gt;: Represents a header for a document or a section. <br />
            &lt;nav&gt;: Defines a set of navigation links. <br />
            &lt;article&gt;: Represents a self-contained content. <br />
            &lt;section&gt;: Defines a section in a document. <br />
            &lt;footer&gt;: Represents a footer for a document or a section.</p>
          <h6>Example :</h6>
          <pre>
            &lt;header&gt;<br />
            &nbsp;&nbsp;&lt;h1&gt;Website Header&lt;/h1&gt;<br />
            &lt;/header&gt;<br />
            &lt;nav&gt;<br />
            &nbsp;&nbsp;&lt;a href=&quot;#&quot;&gt;Home&lt;/a&gt;<br />
            &nbsp;&nbsp;&lt;a href=&quot;#&quot;&gt;About&lt;/a&gt;<br />
            &lt;/nav&gt;<br />
            &lt;article&gt;<br />
            &nbsp;&nbsp;&lt;h2&gt;Article Title&lt;/h2&gt;<br />
            &nbsp;&nbsp;&lt;p&gt;Article content goes here.&lt;/p&gt;<br />
            &lt;/article&gt;<br />
            &lt;section&gt;<br />
            &nbsp;&nbsp;&lt;h2&gt;Section Title&lt;/h2&gt;<br />
            &nbsp;&nbsp;&lt;p&gt;Section content goes here.&lt;/p&gt;<br />
            &lt;/section&gt;<br />
            &lt;footer&gt;<br />
            &nbsp;&nbsp;&lt;p&gt;&amp;copy; 2023 Your Website&lt;/p&gt;<br />
            &lt;/footer&gt;

          </pre>
        </li>









      </ul>






    </div>
  )
}

export default HTMLSession7