import React from 'react'

function HTMLSession2() {
  return (
    <div>
      <h4><strong>1. Installation of VS Code IDE</strong> <br /> </h4>
      <li>Introduction to Visual Studio Code (VS Code).</li>
      <li> Download and install VS Code from the official website.</li><br />


      <h4><strong>2. Add Extension of Live Server</strong></h4>
      <li>Introduction to Live Server as a VS Code extension.</li>
      <li>Install Live Server from the Extensions view in VS Code.</li><br />

      <h4><strong>3. Write a Basic HTML Code and Test in Live Server</strong></h4>
      <li> Open VS Code, create a new ".html" file, and write basic HTML structure.</li><br />
      <li>Add content like headings, paragraphs, and images.</li>
      <li>Test the code using Live Server.</li>

      <h4><strong>4. Code Shortcuts in VS Code and Lorem Text</strong></h4>
      <li>Introduce common VS Code shortcuts (e.g., Save, Copy, Paste, Undo).</li>
      <li>Use Lorem Ipsum text as a placeholder in web development.</li><br />

      <h4><strong>5. Explain Meta Tags</strong></h4>
      <p><strong>Add meta tags</strong> inside the &lt;head&gt; section. Meta tags provide metadata about the HTML document, such as character set and viewport settings.</p>
      <pre>
        <code>
          &lt;!DOCTYPE html&gt;<br />
          &lt;html&gt;<br />
          &nbsp;&nbsp;&lt;head&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;meta charset="UTF-8"&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;meta name="viewport" content="width=device-width, initial-scale=1.0"&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;title&gt;My First HTML Page&lt;/title&gt;<br />
          &nbsp;&nbsp;&lt;/head&gt;<br />
          &nbsp;&nbsp;&lt;body&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;!-- Content goes here --&gt;<br />
          &nbsp;&nbsp;&lt;/body&gt;<br />
          &lt;/html&gt;

        </code>
      </pre>
      <li> <strong>&lt;meta charset="UTF-8"&gt;:</strong> Sets the character encoding for the document to UTF-8.</li>
      <li><strong>&lt;meta name="viewport" content="width=device-width, initial-scale=1.0"&gt;: </strong>Defines the viewport properties for responsive design.</li>

    </div>
  )
}

export default HTMLSession2