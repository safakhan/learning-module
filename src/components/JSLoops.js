import React from 'react'

function JSLoops() {

  return (
    <div>
      <h2>Loops in JavaScript</h2>
      <p>Loops can <strong> execute a block of code repeatedly</strong> while some condition evaluates to true.</p>
      <h5>Different kinds of Loops</h5>
      <p>JavaScript supports different kinds of Loops :</p>
      <ul>
        <li><strong>for</strong> - loops through a block of code a number of times</li>
        <li><strong>while</strong> - loops through a block of code while a specified condition is true</li>
        <li><strong>do-while</strong> - same as while loop but the code is executed atleast once.</li>
        <li><strong>for-in</strong> - loops through the properties of an object</li>
        <li><strong>for-of</strong> - loops through the values of an iterable object</li>
        <li><strong>forEach</strong> - executes a function for each element in the array.</li>

      </ul>
      <h5>JavaScript for loop</h5>
      <p>A for loop is used when you know how many times you need to repeat a certain block of code.<br /> It takes three statements:</p>
      <ul>
        <li>Initialization statement</li>
        <li>Condition Statement</li>
        <li>Increment/Decrement statement.</li>
      </ul>
      <h6>Syntax :</h6>
      <pre>
        for (initialization; testing condition; increment/decrement) &#123;<br />
        &nbsp;&nbsp;&nbsp;&nbsp;statement(s) <br/>
        &#125;
      </pre>
      <h6>Example :</h6>
      <pre>
        <code>
          for (let i = 0; i &lt; 5; i++) &#123;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;text += "The number is " + i ;<br /> 
          &#125;
        </code>
      </pre>
      <h6>Output :</h6>
      <pre>
        The number is 0
        The number is 1
        The number is 2
        The number is 3
        The number is 4
      </pre>

      <h5>JavaScript while Loop</h5>
      <p>A while loop is used when you don’t know how many times you need to repeat a block of code, but you know the condition that will end the loop.</p>
      <h6>Syntax :</h6>
      <pre>
        <code>
          while (condition) &#123;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;// code block to be executed<br />
          &#125;
        </code>
      </pre>
      <h6>Example :</h6>
      <pre>
        <code>
          let i = 0;<br />
          while (i &lt; 6) &#123;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;console.log(i);<br />
          &nbsp;&nbsp;&nbsp;&nbsp;i++;<br />
          &#125;
        </code>
      </pre>
      <h6>Output :</h6>
      <pre>
        0 <br />
        1 <br />
        2 <br />
        3 <br />
        4 <br />
        5
      </pre>
      <h5>JavaScript do-while Loop</h5>
      <p>A do-while loop is similar to a while loop, but the block of code is executed at least once, even if the condition is false.</p>
      <h6>Syntax :</h6>
      <pre>
        <code>
          do &#123;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;// code block to be executed<br />
          &#125; while (condition);
        </code>
      </pre>
      <h6>Example :</h6>
      <pre>
        <code>
          do &#123;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;text += "The number is " + i;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;i++;<br />
          &#125; while (i &lt; 10);
        </code>
      </pre>

      <h6>Output :</h6>
      <pre>
        The number is 0 <br />
        The number is 1 <br />
        The number is 2 <br />
        The number is 3 <br />
        The number is 4 <br />
        The number is 5 <br />
        The number is 6 <br />
        The number is 7 <br />
        The number is 8 <br />
        The number is 9
      </pre>
      <h5>JavaScript for-in Loop</h5>
      <p>A for-in loop is used to loop through the properties of an object.</p>
      <h6>Syntax :</h6>
      <pre>
        <code>
          for (key in object) &#123;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;// code block to be executed<br />
          &#125;
        </code>
      </pre>

      <h6>Example :</h6>
      <pre>
        <code>
          const obj = &#123;a: 1, b: 4, c: 7&#125;;<br />
          <br />
          for (let prop in obj) &#123;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;console.log(prop + ': ' + obj[prop]);<br />
          &#125;
        </code>
      </pre>

      <h6>Output :</h6>
      <pre>
        a: 1 <br />
        b: 4 <br />
        c: 7
      </pre>
      <h5>JavaScript for-of Loop </h5>
      <p>A for-of loop is used to loop through the values of an iterable object (such as an array).</p>
      <h6>Syntax :</h6>
      <pre>
        <code>
          for (variable of iterable) &#123;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;// code block to be executed<br />
          &#125;
        </code>
      </pre>

      <h6>Example :</h6>
      <pre>
        <code>
          const arr = [1, 2, 3];<br />
          <br />
          for (let val of arr) &#123;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;console.log(val);<br />
          &#125;
        </code>
      </pre>

      <h6>Output :</h6>
      <pre>
        1 <br />
        2 <br />
        3
      </pre>

      <h5>JavaScript forEach Loop</h5>
      <p>A forEach loop is a method on arrays that executes a function for each element in the array.</p>
      <h6>Syntax :</h6>
      <pre>
        <code>
          array.forEach(function(currentValue, index, array) &#123;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;// code block to be executed for each element<br />
          &#125;);
        </code>
      </pre>


      <h6>Example :</h6>
      <pre>
        <code>
          const arr = [1, 2, 3];<br />
          arr.forEach(val =&gt; console.log(val));<br />
        </code>
      </pre>

      <h6>Output :</h6>
      <pre>
        1 <br />
        2 <br />
        3
      </pre>
    </div>
  )
}

export default JSLoops