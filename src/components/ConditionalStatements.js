import React from 'react'

function ConditionalStatements() {

   
  console.log("ConditionalStatements component is rendering."); 
  return (
    <div>
    <h2>JavaScript Conditional Statements</h2>
    <p>
      Conditional statements are used to <strong>perform different actions based on different conditions.</strong></p>
    <p>In JavaScript we have the following conditional statements:</p>
    <ul>
            <li><strong>if : </strong>specifies a block of code to be executed if the specified condition is true</li>
            <li><strong>else : </strong>specifies a block of code to be executed if the same condition is false</li>
            <li><strong>else if : </strong>specifies a new condition to be tested if the previous one is false</li>
            <li><strong>switch : </strong>specifies many alternative blocks of code to be executed</li>
          </ul>
          <h5>The if Statement :</h5>
          <p>Specifies a block of code to be executed if the specified condition is true.</p>
          <h6>Syntax :</h6>
          <pre>
            <code>
              if (condition)  &#123;<br />
              &nbsp;&nbsp;&nbsp;//  block of code to be executed if the condition is true <br/>
              &#125;
            </code>
          </pre>
          <h6>Example :</h6>
          <pre>
            <code>
              let i = 20; <br />
            if (i > 15) &#123;<br />
            &nbsp;&nbsp;&nbsp;console.log("i is greater than 15");<br />
              &#125;
            </code>
          </pre>
          <h6>Output :</h6>
          <pre>
            i is greater than 15
          </pre>
          <h5>The else Statement :</h5>
          <p>Specifies a block of code to be executed if the condition is false.</p>
          <h6>Syntax :</h6>
          <pre>
            <code>
              if (condition)  &#123; <br />
              &nbsp;&nbsp;&nbsp;//  block of code to be executed if the condition is true <br/>
              &#125;  <br />
              else &#123;<br />
              &nbsp;&nbsp;&nbsp; //  block of code to be executed if the condition is false <br/>
              &#125;
            </code>
          </pre>
          <h6>Example :</h6>
          <pre>
            <code>
              let i = 10;<br />
            if (i > 15) &#123;<br />
            &nbsp;&nbsp;&nbsp;console.log("i is greater than");
              &#125;<br />
              else &#123;<br />
              &nbsp;&nbsp;&nbsp;console.log("i is  less than 15");<br />
              &#125;
            </code>
          </pre>
          <h6>Output :</h6>
          <pre>
            i is less than 15
          </pre>
          <h5>The else if Statement :</h5>
          <p>Specifies a new condition if the first condition is false.</p>
          <h6>Syntax :</h6>
          <pre>
            <code>
              if (condition1)  &#123;<br />
              &nbsp;&nbsp;&nbsp; //  block of code to be executed if the condition1 is true <br/>
              &#125;<br />
              else if(condition2) &#123;<br />
              &nbsp;&nbsp;&nbsp;//  lock of code to be executed if the condition1 is false and condition2 is true <br/>
              &#125;<br />
              else &#123;<br />
              &nbsp;&nbsp;&nbsp; // block of code to be executed if the condition1 is false and condition2 is false <br/>
              &#125;
            </code>
          </pre>
          <h6>Example :</h6>
          <pre>
            <code>
              let i = 20;<br />

              if (i == 10) <br/>
              &nbsp;&nbsp;&nbsp;console.log("i is 10");<br />
              else if (i == 15)<br/>
              &nbsp;&nbsp;&nbsp;console.log("i is 15");<br />
              else if (i == 20)<br/>
              &nbsp;&nbsp;&nbsp;console.log("i is 20");<br />
              else <br/>
              &nbsp;&nbsp;&nbsp;console.log("i is not present");
            </code>
          </pre>
          <h6>Output :</h6>
          <pre>
            i is 20
          </pre>

          <h5>The switch Statement :</h5>
          <p>Use the switch statement to select one of many code blocks to be executed..</p>
          <h6>Syntax :</h6>
          <pre>
            <code>
              switch(expression) &#123;<br />
              &nbsp;&nbsp;case value1: <br/>
              &nbsp;&nbsp;&nbsp;&nbsp; // code block <br/>
              &nbsp;&nbsp;break;<br />
              &nbsp;&nbsp;case value2: <br />
              &nbsp;&nbsp;&nbsp;&nbsp; // code block <br/>
              &nbsp;&nbsp;break; <br />
              &nbsp;&nbsp;default: <br />
              &nbsp;&nbsp;&nbsp;&nbsp;// code block <br/>
              &#125;
            </code>
          </pre>

          <h6>Example :</h6>
          <pre>
            <code>
              let i = 9;<br />

              switch (i) &#123;<br />
              &nbsp;&nbsp;case 0:<br/>
              &nbsp;&nbsp;&nbsp;&nbsp;console.log("i is zero.");<br />
              &nbsp;&nbsp;break;<br />
              &nbsp;&nbsp;case 1:<br/>
              &nbsp;&nbsp;&nbsp;&nbsp;console.log("i is one.");<br />
              &nbsp;&nbsp;break;<br />
              &nbsp;&nbsp;case 2: <br/>
              &nbsp;&nbsp;&nbsp;&nbsp;console.log("i is two.");<br />
              &nbsp;&nbsp;break; <br />
              &nbsp;&nbsp;default: <br />
              &nbsp;&nbsp;&nbsp;&nbsp;console.log("i is greater than 2.");
              &#125;
            </code>
          </pre>
          <h6>Output :</h6>
          <pre>
          i is greater than 2.
          </pre>


  </div>
  )
}

export default ConditionalStatements