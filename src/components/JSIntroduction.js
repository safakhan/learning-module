import React from 'react'

function JSIntroduction() {
  return (
    <div>
        <div>
                    <h2> <strong>Introduction to JavaScript</strong> <br /> <br /> </h2>
                    <h3>  What is JavaScript ?<br /> </h3>
                    <p>
                        JavaScript is a <strong>lightweight, cross-platform, single-threaded</strong>, and
                        <strong> interpreted compiled programming language</strong>. It is also known as the
                        <strong>scripting language</strong> for webpages. It is well-known for the
                        development of web pages, and many non-browser environments also
                        use it. <br />
                        <br />
                        JavaScript is a <strong>weakly typed language (dynamically typed)</strong>.
                        JavaScript can be used for Client-side developments as well as
                        Server-side developments. JavaScript is both an imperative and
                        declarative type of language. JavaScript contains a standard
                        library of objects, like Array, Date, and Math, and a core set of
                        language elements like operators, control structures, and
                        statements.
                    </p>

                </div>
    </div>
  )
}

export default JSIntroduction