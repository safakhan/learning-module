import React from 'react'

function HTMLSession6() {
  return (
    <div>
      <h4><strong>1. Anchor Tag</strong></h4>
      <p>The anchor tag (&lt;a&gt;) is used to create hyperlinks in HTML. It is commonly used to link to other web pages, documents, or resources.</p>
      <h6>Usage:</h6>
      <pre> &lt;a href=&quot;https://www.example.com&quot;&gt;VisitExample.com&lt;/a&gt;</pre>
      <h6>Example :</h6>
      <pre>&lt;p&gt;For more information, &lt;a href=&quot;https://www.example.com&quot;&gt;visit our website&lt;/a&gt;.&lt;/p&gt;</pre>

      <h4><strong>2. Form Tag</strong></h4>
      <p>The form tag (&lt;form&gt;) is used to create an HTML form for user input. It can contain various form elements like text fields, buttons, checkboxes, and more.</p>
      <h6>Usage :</h6>
      <pre>
        <code>
          &lt;form action=&quot;/submit&quot; method=&quot;post&quot;&gt;<br />
          &nbsp;&nbsp;&lt;!-- Form elements go here --&gt;<br />
          &nbsp;&nbsp;&lt;input type=&quot;text&quot; name=&quot;username&quot; placeholder=&quot;Enter your username&quot;&gt;<br />
          &nbsp;&nbsp;&lt;input type=&quot;password&quot; name=&quot;password&quot; placeholder=&quot;Enter your password&quot;&gt;<br />
          &nbsp;&nbsp;&lt;button type=&quot;submit&quot;&gt;Submit&lt;/button&gt;<br />
          &lt;/form&gt;

        </code>
      </pre>
      <h6>Example :</h6>
      <pre>
        <code>
          &lt;!DOCTYPE html&gt;<br />
          &lt;html lang=&quot;en&quot;&gt;<br />
          &lt;head&gt;<br />
          &nbsp;&nbsp;&lt;meta charset=&quot;UTF-8&quot;&gt;<br />
          &nbsp;&nbsp;&lt;meta http-equiv=&quot;X-UA-Compatible&quot; content=&quot;IE=edge&quot;&gt;<br />
          &nbsp;&nbsp;&lt;meta name=&quot;viewport&quot; content=&quot;width=device-width, initial-scale=1.0&quot;&gt;<br />
          &nbsp;&nbsp;&lt;title&gt;Form Elements&lt;/title&gt;<br />
          &lt;/head&gt;<br />
          &lt;body&gt;<br />
          &nbsp;&nbsp;&lt;h1&gt;Registration Form&lt;/h1&gt;<br />
          &nbsp;&nbsp;&lt;form action=&quot;Day01Tags.html&quot;&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;label &gt;First Name&lt;/label&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type=&quot;text&quot; required&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;label&gt;Last Name&lt;/label&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type=&quot;text&quot; required&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;&lt;br&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;label &gt;Email &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/label&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type=&quot;email&quot; required&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;&lt;br&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;label &gt;Password&lt;/label&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type=&quot;password&quot; required&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;label &gt;Course Join Date&lt;/label&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type=&quot;date&quot; required&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;label &gt;Course:&lt;/label&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;label &gt;Python&lt;/label&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type=&quot;checkbox&quot; &gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;label &gt;SQL&lt;/label&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type=&quot;checkbox&quot;&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;label &gt;HTML&lt;/label&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type=&quot;checkbox&quot;&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;label &gt;CSS&lt;/label&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type=&quot;checkbox&quot;&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;label &gt;Course Completion&lt;/label&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;label &gt;Yes&lt;/label&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type=&quot;radio&quot; name=&quot;course completion&quot;&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;label &gt;No&lt;/label&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type=&quot;radio&quot; name=&quot;course completion&quot;&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;label &gt;Upload Resume&lt;/label&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type=&quot;file&quot; required&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp

        </code>
      </pre>
    </div>
  )
}

export default HTMLSession6