import React from 'react'

function FOP() {
    return (
        <div>
            <h2>FOUNDATIONS OF PROGRAMMING</h2><br />
            <h6>1. Program to find sum of first n natural numbers :</h6>
            <pre>
                <code>
                    let num = 5;<br />
                    let sum = 0;<br />

                    for(let i = 1; i &lt;= num5; i++) &#123;<br />
                    &nbsp;&nbsp;sum = sum + i;<br />
                    &#125;<br />

                    console.log("sum is " + sum);<br />
                </code>
            </pre>
            <h6>Output</h6>
            <pre>sum is 15</pre> <br />
            <h6>2. Write a program to determine whether a number is even or odd</h6>
            <pre>
                <code>
                    let num = 8;<br />

                    if(num2 % 2 == 0) &#123;<br />
                    &nbsp;&nbsp;console.log(num +" is an even number");<br />
                    &#125; else &#123;<br />
                    &nbsp;&nbsp;console.log("Odd");<br />
                    &#125;<br />
                </code>
            </pre>
            <h6>output</h6>
            <pre>8 is an even number</pre> <br />
            <h6>3. Write a program to determine largest of 3 numbers :</h6>
            <pre>
                <code>
                    let d = 10;<br />
                    let e = 20;<br />
                    let f = 30;<br />

                    if (d &gt; e &amp;&amp; d &gt; f) &#123;<br />
                    &nbsp;&nbsp;console.log(a +" is greater");<br />
                    &#125; else if (e &gt; d &amp;&amp; e &gt; f) &#123;<br />
                    &nbsp;&nbsp;console.log(e +" is greater");<br />
                    &#125; else &#123;<br />
                    &nbsp;&nbsp;console.log(f +" is greater");<br />
                    &#125;
                </code>
            </pre>
            <h6>Output</h6>
            <pre>30 is greater</pre><br />
            <h6>4. Program to find square of a number</h6>
            <pre>
                <code>
                    function square(num) &#123;<br />
                    &nbsp;&nbsp;let square = num * num;<br />
                    &nbsp;&nbsp;return square;<br />
                    &#125;<br />

                    console.log(square(3));<br />
                </code>
            </pre>
            <h6>Output</h6>
            <pre>9</pre><br />
            <h6>5. Program to find reverse of a number</h6>
            <pre>
                <code>
                    function reverse(val) &#123;<br />
                    &nbsp;&nbsp;let reversenum = 0;<br />
                    &nbsp;&nbsp;while (val != 0) &#123;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;reversenum = reversenum * 10 + val % 10;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;val = Math.floor(val / 10);<br />
                    &nbsp;&nbsp;&#125;<br />
                    &nbsp;&nbsp;return reversenum;<br />
                    &#125; <br /><br />

                    console.log(reverse(1234));<br />
                </code>
            </pre>
            <h6>Output</h6>
            <pre>4321</pre> <br />
            <h6>6. Program to check whether a number is palindrome or not :</h6>
            <pre>
                <code>
                    function IsPalindrome(num) &#123;<br /><br />
                    &nbsp;&nbsp;return (num == reverse(num)) ? "palindrome" : "not palindrome";<br />
                    &#125;<br /> <br />

                    console.log(IsPalindrome(1441));<br />
                </code>
            </pre>
            <h6>Output</h6>
            <pre>palindrome</pre><br />
            <h6>7. Program to check whether a number or prime or not :</h6>
            <pre>
                <code>
                    function IsPrime(num) &#123;<br />
                    &nbsp;&nbsp;let count = 0;<br />
                    &nbsp;&nbsp;for (let i = 2; i * i &lt;= num; i++) &#123;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;if (num % i == 0) &#123;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;count++;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;break;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&#125;<br />
                    &nbsp;&nbsp;if (count == 0)<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;return "Prime";<br />
                    &nbsp;&nbsp;else<br />
                    &nbsp;&nbsp;return "Not a prime number";<br />
                    &#125;<br />

                    console.log(IsPrime(13));<br />
                </code>
            </pre>
            <h6>Output</h6>
            <pre>Prime</pre> <br />
            <h6>8. Program to find factorial of a number</h6>
            <pre>
                <code>
                    function factorial(num) &#123;<br />
                    &nbsp;&nbsp;let result = 1;<br />
                    &nbsp;&nbsp;for (let i = 2; i &lt;= num; i++) &#123;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;result = result * i;<br />
                    &nbsp;&nbsp;&#125;<br />
                    &nbsp;&nbsp;return result;<br />
                    &#125; <br />

                    console.log(factorial(3));<br />
                </code>
            </pre>
            <h6>Output</h6>
            <pre>6</pre><br />
            <h6>9. Program to find factor of a given number :</h6>
            <pre>
                <code>
                    function factorsOfANumber(num) &#123;<br />
                    &nbsp;&nbsp;factors = [];<br />
                    &nbsp;&nbsp;for (let i = 1; i &lt;= num; i++) &#123;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;if (num % i == 0)<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;factors.push(i);<br />
                    &nbsp;&nbsp;&#125;<br />
                    &nbsp;&nbsp;return factors;<br />
                    &#125;<br /><br />

                    console.log(factorsOfANumber(12));<br />
                </code>
            </pre>
            <h5>Output</h5>
            <pre>[1, 2, 3, 4, 6, 12]</pre> <br />
            <h6>10. Program to find maximum in an array</h6>
            <pre>
                <code>
                    function maxInArray(numbers) &#123;<br />
                    &nbsp;&nbsp;max = numbers[0];<br />
                    &nbsp;&nbsp;for (let i = 1; i &lt; numbers.length; i++) &#123;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;if (numbers[i] &gt; max)<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;max = numbers[i];<br />
                    &nbsp;&nbsp;&#125;<br />
                    &nbsp;&nbsp;return max;<br />
                    &#125; <br /><br />

                    let array = [1, 12, 56, 34, 89, 90, 1000];<br />
                    console.log(maxInArray(array));<br />
                </code>
            </pre>
            <h6>Output</h6>
            <pre>1000</pre>
        </div>
    )
}

export default FOP