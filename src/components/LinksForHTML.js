import React from 'react'
import { Link } from 'react-router-dom';

function LinksForHTML() {
    const linkStyle = {
        color: "black",
      };
  return (
    <div className='quicklinks'>
        <Link style={linkStyle} to="/nav/html/" >HTML Session 1</Link> <br/><br/>
        <Link style={linkStyle} to="/nav/html/session2" >HTML Session 2</Link><br/><br/>
        <Link style={linkStyle} to="/nav/html/session3" >HTML Session 3</Link><br/><br/>
        <Link style={linkStyle} to="/nav/html/session4" >HTML Session 4</Link><br/><br/>
        <Link style={linkStyle} to="/nav/html/session5" >HTML Session 5</Link><br/><br/>
        <Link style={linkStyle} to="/nav/html/session6" >HTML Session 6</Link><br/><br/>
        <Link style={linkStyle} to="/nav/html/session7" >HTML Session 7</Link><br/><br/>




    </div>
  )
}

export default LinksForHTML