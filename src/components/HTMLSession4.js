import React from 'react'

function HTMLSession4() {
  return (
    <div>
      <h4><strong>1. List Tags (Ordered and Unordered)</strong></h4>
      <strong>Introduction to list tags for creating ordered and unordered lists.</strong>
      <p>Usage of &lt;ol&gt; (Ordered List) and &lt;ul&gt; (Unordered List) tags.</p>
      <h6>Example :</h6>
      <pre>
        <code>
          &lt;ol&gt;
          &lt;li&gt;Item 1&lt;/li&gt;
          &lt;li&gt;Item 2&lt;/li&gt;
          &lt;li&gt;Item 3&lt;/li&gt;
          &lt;/ol&gt;
          &lt;ul&gt;
          &lt;li&gt;Item A&lt;/li&gt;
          &lt;li&gt;Item B&lt;/li&gt;
          &lt;li&gt;Item C&lt;/li&gt;
          &lt;/ul&gt;
        </code>
      </pre>
      <h4><strong>2. Image Tag</strong></h4>
      <p>Introduction to the &lt;img&gt; tag for embedding images in HTML.</p>
      <p>Usage of the src attribute to specify the image source.</p>
      <h6>Example: </h6>
      <pre>
        <code>
          &lt;img src=&quot;image.jpg&quot; alt=&quot;Description of the image&quot;&gt;
        </code>
      </pre>
      <h4><strong>3. Adding Comments</strong></h4>
      <p>Explanation of adding comments in HTML for documentation and clarification. <br /> Usage of &lt;!-- ... --&gt; to add comments.</p>
      <h6>Example:</h6>
      <pre>&lt;!-- This is a comment --&gt;</pre>
      <h4><strong>4. Table Tag</strong></h4>
      <p>Introduction to the &lt;table&gt; tag for creating tables in HTML. <br /> Usage of &lt;tr&gt; (table row), &lt;th&gt; (table header), and &lt;td&gt; (table data) tags.</p>
      <h6>Example: </h6>
      <pre>
        <code>
          &lt;table&gt;<br />
          &nbsp;&nbsp;&lt;tr&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;th&gt;Header 1&lt;/th&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;th&gt;Header 2&lt;/th&gt;<br />
          &nbsp;&nbsp;&lt;/tr&gt;<br />
          &nbsp;&nbsp;&lt;tr&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;td&gt;Data 1&lt;/td&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;td&gt;Data 2&lt;/td&gt;<br />
          &nbsp;&nbsp;&lt;/tr&gt;<br />
          &lt;/table&gt;

        </code>
      </pre>

      <h4><strong>5. Table Tag</strong></h4>
      <p>Introduction to form tags for creating interactive forms. <br /> Usage of &lt;form&gt;, &lt;input&gt;, &lt;textarea&gt;, &lt;select&gt;, and other related tags.</p>
      <h6>Example 1:</h6>
      <pre>
        <code>
          &lt;form action=&quot;/submit&quot; method=&quot;post&quot;&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;label for=&quot;username&quot;&gt;Username:&lt;/label&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type=&quot;text&quot; id=&quot;username&quot; name=&quot;username&quot;&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type=&quot;submit&quot; value=&quot;Submit&quot;&gt;<br />
          &lt;/form&gt;

        </code>
      </pre>
      <h6>Example 2:</h6>
      <pre>
        <code>
          &lt;!DOCTYPE html&gt;<br />
          &lt;html lang=&quot;en&quot;&gt;<br />
          &lt;head&gt;<br />
          &nbsp;&nbsp;&lt;meta charset=&quot;UTF-8&quot;&gt;<br />
          &nbsp;&nbsp;&lt;meta http-equiv=&quot;X-UA-Compatible&quot; content=&quot;IE=edge&quot;&gt;<br />
          &nbsp;&nbsp;&lt;meta name=&quot;viewport&quot; content=&quot;width=device-width, initial-scale=1.0&quot;&gt;<br />
          &nbsp;&nbsp;&lt;title&gt;Form Elements&lt;/title&gt;<br />
          &lt;/head&gt;<br />
          &lt;body&gt;<br />
          &nbsp;&nbsp;&lt;h1&gt;Registration Form&lt;/h1&gt;<br />
          &nbsp;&nbsp;&lt;form action=&quot;Day01Tags.html&quot;&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;label &gt;First Name&lt;/label&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type=&quot;text&quot; required&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;label&gt;Last Name&lt;/label&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type=&quot;text&quot; required&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;&lt;br&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;label &gt;Email &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/label&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type=&quot;email&quot; required&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;&lt;br&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;label &gt;Password&lt;/label&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type=&quot;password&quot; required&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;label &gt;Course Join Date&lt;/label&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type=&quot;date&quot; required&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;label &gt;Course:&lt;/label&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;label &gt;Python&lt;/label&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type=&quot;checkbox&quot; &gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;label &gt;SQL&lt;/label&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type=&quot;checkbox&quot;&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;label &gt;HTML&lt;/label&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type=&quot;checkbox&quot;&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;label &gt;CSS&lt;/label&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type=&quot;checkbox&quot;&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;label &gt;Course Completion&lt;/label&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;label &gt;Yes&lt;/label&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type=&quot;radio&quot; name=&quot;course completion&quot;&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;label &gt;No&lt;/label&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type=&quot;radio&quot; name=&quot;course completion&quot;&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;label &gt;Upload Resume&lt;/label&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type=&quot;file&quot; required&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;button type=&quot;submit&quot;&gt;Submit&lt;/button&gt;<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type=&quot;reset&quot;&gt;<br />
          &nbsp;&nbsp;&lt;/form&gt;<br />
          &lt;/body&gt;<br />
          &lt;/html&gt;

        </code>
      </pre>
    </div>
  )
}

export default HTMLSession4