import React from 'react'
import { Link, useNavigate } from 'react-router-dom'
import logo2 from '../logo2.png'
import {useState} from 'react'
import axios from 'axios'
import logo5 from '../logo5.png'


function Register() {
  const main = {
    fontFamily: 'Arial, sans-serif',
    backgroundColor: '#f3f3f3'
  }
  const header = {
    backgroundColor: 'white',
    color: '#fff',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '10px 20px'
  }
  const linkStyle = {
    color: '#5e4e9b',
    textDecoration: 'none',
    marginLeft: '20px',
    fontSize: '20px'
  }

  const container = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: 'calc(100vh - 80px)',
}
const registerContainer = {
    backgroundColor: '#fff',
    borderRadius: '5px',
    padding: '20px',
    boxShadow: '0 0 10px rgba(0, 0, 0, 0.1)',
    width: '800px',
    textAlign: 'center',
}
const input = {
    width: '100%',
    padding: '10px',
    margin: '10px 3px',
    border: '1px solid #ccc',
    borderRadius: '3px',
    fontSize: '16px',
}
const button = {
    backgroundColor: '#5e4e9b',
    color: '#fff',
    padding: '10px 20px',
    border: 'none',
    borderRadius: '3px',
    cursor: 'pointer',
    fontSize: '16px',
    width:'300px',
    margin:'12px 0px'
}
const column ={
  display:'flex',
  justifyContent:'space-between',
  alignItems:'center'
}
const navigate = useNavigate()
const [userdata,setUserdata] = useState({
  fname:'',
  lname:'',
  email:'',
  phone:'',
  country:'',
  city:'',
  password:'',
  gender:''
})
const handleInput = (event)=>{
      event.preventDefault()
      const {name,value} = event.target
      setUserdata({
        ...userdata,
        [name] : value
      })
}

const handleRegister = async (event)=>{
  event.preventDefault()
  let errormsg = document.getElementById('error')
  try{
  const response = await axios.post('http://localhost:8000/register', {...userdata})
  if(response.data.message=="Registration Successful"){
    navigate('/login')
  }else if(response.data.message=="Email already Registered"){
    errormsg.innerHTML='Email already Registered'
  }
  else{
    errormsg.innerHTML='Registration Failed'

  }
}catch(error){
  console.log(error)
}
}

  return (
    <div style={main}>
      <header style={header}>
        <div className="logo img-fluid">  
        <Link to='/' style={linkStyle}><img src={logo2} alt="Logo" /></Link>
        </div>
        <nav>
          <Link to='/login' style={linkStyle}>Sign In</Link>
          <Link to='/register' style={linkStyle}>Register</Link>
        </nav>
      </header>

      <main style={container}>
        <div className="login-container" style={registerContainer}>
          <h1 style={{ fontSize: '24px', marginBottom: '20px', color: '#5e4e9b' }}>Welcome to TalentSprint</h1>
          <form onSubmit={handleRegister}>
            <p id='error' style={{color:'red'}}></p>
            <div style={column}>
              <input type="text" placeholder='First Name' name='fname' value={userdata.fname} style={input} onChange={handleInput}/>
              <input type="text" placeholder='Last Name' name='lname' value={userdata.lname} style={input} onChange={handleInput}/>
            </div>
            {/* <input type="text" placeholder='Username' style={input}/> */}
            <div style={column}>
              <input type="email" placeholder='Email' name='email' value={userdata.email} style={input} onChange={handleInput}/>
              <input type="text" placeholder='Phone Number' name='phone' value={userdata.phone} style={input} onChange={handleInput}/>
            </div>
            <div style={column}>
              <input type="text" placeholder='Country' name='country' value={userdata.country} style={input} onChange={handleInput}/>
              <input type="text" placeholder='City' name='city' value={userdata.city} style={input} onChange={handleInput}/>
            </div>
            <div style={column}>
              <input type="password" placeholder='Password' name='password' value={userdata.password} required style={input} onChange={handleInput}/>
              {/* <input type="password" placeholder='Confirm Password' style={input}/> */}
            </div>
            <div style={column}>
               <label htmlFor="gender">Gender</label>
               <label htmlFor="male"><input type="radio" name="gender" id="male" value="Male" onChange={handleInput} /> Male</label>
               <label htmlFor="female"><input type="radio" name="gender" id="female" value="Female" onChange={handleInput}/> Female</label>
               <label htmlFor="others"><input type="radio" name="gender" id="others" value="others" onChange={handleInput} /> Others</label>
            </div>            
            <button style={button} type="submit">Create Account</button>
          </form>
          <p>already have an account? <Link to='/login'>Sign In</Link> </p>

        </div>
      </main>
    </div>
  )
}

export default Register